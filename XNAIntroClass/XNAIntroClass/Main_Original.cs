using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNAIntroClass
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main_Original : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Vector3 cameraPosition;
        private Vector3 cameraTarget;
        private Vector3 cameraUp;

        private float fieldOfView;
        private float aspectRatio;
        private float nearPlaneDistance;
        private float farPlaneDistance;

        private Texture2D torusModelTexture;
        private Model torusModel;
        private Vector3 torusModelDiffuseColor;

        private Vector3 translation;
        private Vector3 rotationInDegrees;
        private Vector3 scale;

        private Matrix view;
        private Matrix projection;

        private BasicEffect torusModelEffect;

        public Main_Original()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //Step 1: Set the screen resoltuion
            #region Screen Resolution
            graphics.PreferredBackBufferWidth = 640;
            graphics.PreferredBackBufferHeight = 480;
            graphics.ApplyChanges();
            #endregion

            //Step 2: Load Resources and set position, orientation and scale
            #region Model Specific
            this.torusModelDiffuseColor = Color.Red.ToVector3();
            this.torusModelTexture = Content.Load<Texture2D>("Assets/Textures/checkerboard");
            this.torusModel = Content.Load<Model>("Assets/Models/torus");
           

            //Position and Orientation
            this.translation = new Vector3(0, 0, 0);
            this.rotationInDegrees = new Vector3(0, 0, 0);
            this.scale = 0.05f * new Vector3(1, 1, 1);
            #endregion

            //Step 3: Tell the game where the camera is and what its properties are
            #region Camera specific
            //Camera Position
            this.cameraPosition = new Vector3(0, 0, 5);
            this.cameraTarget = Vector3.Zero;
            this.cameraUp = Vector3.Up;

            //Camera Projection Properties
            this.fieldOfView = MathHelper.PiOver4;
            this.aspectRatio = 4.0f / 3.0f;
            this.nearPlaneDistance = 1;
            this.farPlaneDistance = 1000;

            //Create the two matrices that encapsulate the camera
            this.view = Matrix.CreateLookAt(cameraPosition, cameraTarget, cameraUp);
            //this.projection = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearPlaneDistance, farPlaneDistance);
            //this.projection = Matrix.CreateOrthographic(8, 6, nearPlaneDistance, farPlaneDistance);
            this.projection = Matrix.CreateOrthographicOffCenter(-2, 6, -3, 3, nearPlaneDistance, farPlaneDistance);
            #endregion

            //Step 4: Create a simple shader
            #region Graphics card specific code
            this.torusModelEffect = new BasicEffect(graphics.GraphicsDevice);
            this.torusModelEffect.TextureEnabled = true;
            this.torusModelEffect.EnableDefaultLighting();
            this.torusModelEffect.PreferPerPixelLighting = true;
            this.torusModelEffect.SpecularPower = 256;
            //this.torusModelEffect.SpecularColor = new Vector3(0, 1.0f, 0);
            #endregion

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            this.rotationInDegrees.Y -= 1;

            KeyboardState ksState = Keyboard.GetState();

            if(ksState.IsKeyDown(Keys.W))
            {
                this.cameraPosition -= 0.05f*Vector3.UnitZ;
                this.view = Matrix.CreateLookAt(cameraPosition, cameraTarget, cameraUp);
            }

            else if (ksState.IsKeyDown(Keys.S))
            {
                this.cameraPosition += 0.05f * Vector3.UnitZ;
                this.view = Matrix.CreateLookAt(cameraPosition, cameraTarget, cameraUp);
            }

            else if (ksState.IsKeyDown(Keys.A))
            {
                this.cameraPosition -= 0.05f * Vector3.UnitX;
                this.cameraTarget -= 0.05f * Vector3.UnitX;
                this.view = Matrix.CreateLookAt(cameraPosition, cameraTarget, cameraUp);
            }

            else if (ksState.IsKeyDown(Keys.D))
            {
                this.cameraPosition += 0.05f * Vector3.UnitX;
                this.cameraTarget += 0.05f * Vector3.UnitX;
                this.view = Matrix.CreateLookAt(cameraPosition, cameraTarget, cameraUp);
            }



            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //Step 5: Apply the variables on the GFX card - the view, projection and world matrices
            this.torusModelEffect.View = this.view;
            this.torusModelEffect.Projection = this.projection;

            //set the texture and bleed color
            this.torusModelEffect.DiffuseColor = this.torusModelDiffuseColor;
            this.torusModelEffect.Texture = this.torusModelTexture;
            //set these variables in the storage registers in the GFX card
            this.torusModelEffect.CurrentTechnique.Passes[0].Apply();

            //Step 6: For each mesh in the model apply the effect

            foreach (ModelMesh mesh in this.torusModel.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = this.torusModelEffect;
                }

                //Step 7: Generate world matrix in ISRoT order Identity, Scale, Rotation, Translation
                this.torusModelEffect.World = Matrix.Identity * Matrix.CreateScale(this.scale) *
                    Matrix.CreateRotationX(MathHelper.ToRadians(this.rotationInDegrees.X)) *
                    Matrix.CreateRotationY(MathHelper.ToRadians(this.rotationInDegrees.Y)) *
                    Matrix.CreateRotationZ(MathHelper.ToRadians(this.rotationInDegrees.Z)) *
                    Matrix.CreateTranslation(this.translation);

                //Step 8: Call the draw - pass the vertices to the graphics card
                mesh.Draw();
            }


            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
