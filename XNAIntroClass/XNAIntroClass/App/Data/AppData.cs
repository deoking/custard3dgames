﻿/* Store common hard-coded values within the game
 * e.g key mappings, mouse sensitivity etc*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XNAIntroClass.Data
{
    public sealed class LerpSpeed
    {
        private static readonly float SpeedMultiplier = 2;
        public static readonly float VerySlow = 0.05f;
        public static readonly float Slow = SpeedMultiplier * VerySlow;
        public static readonly float Medium = SpeedMultiplier * Slow;
        public static readonly float Fast = SpeedMultiplier * Medium;
        public static readonly float VeryFast = SpeedMultiplier * Fast;
    }

    public class AppData
    {
        #region Common
        public static readonly int IndexMoveForward = 0;
        public static readonly int IndexMoveBackward = 1;
        public static readonly int IndexRotateLeft = 2;
        public static readonly int IndexRotateRight = 3;
        public static readonly int IndexMoveJump = 4;
        public static readonly int IndexMoveCrouch = 5;
        public static readonly int IndexStrafeLeft = 6;
        public static readonly int IndexStrafeRight = 7;
        #endregion

        #region JigLibX
        public static readonly Vector3 Gravity = -10 * Vector3.UnitY;
        public static readonly Vector3 BigGravity = 5 * Gravity;

        #endregion

        #region Camera
        public static readonly int CurveEvaluationPrecision = 4;

        public static readonly float CameraRotationSpeed = 0.0125f;
        public static readonly float CameraMoveSpeed = 0.6f;
        public static readonly float CameraStrafeSpeed = 0.6f * CameraMoveSpeed;

        public static readonly float CollidableCameraCapsuleRadius = 2;
        public static readonly float CollidableCameraViewHeight = 15; //how tall is the first person player?
        public static readonly float CollidableCameraMass = 10;

        public static readonly Keys[] CameraMoveKeys = { Keys.W, Keys.S, Keys.A, Keys.D,
                                         Keys.Space, Keys.C, Keys.LeftShift, Keys.RightShift};
        public static readonly Keys[] CameraMoveKeys_Alt1 = { Keys.T, Keys.G, Keys.F, Keys.H };

        public static readonly float CameraThirdPersonScrollSpeedDistanceMultiplier = 0.00125f;
        public static readonly float CameraThirdPersonScrollSpeedElevatationMultiplier = 0.01f;
        public static readonly float CameraThirdPersonDistance = 20;
        public static readonly float CameraThirdPersonElevationAngleInDegrees = 160;

        //JigLib related collidable camera properties
        public static readonly float CollidableCameraJumpHeight = 12;
        public static readonly float CollidableCameraMoveSpeed = 0;
        public static readonly float CollidableCameraStrafeSpeed = 0.6f * CollidableCameraMoveSpeed;
        

        #endregion

        #region Security Camera
        public static readonly float SecurityCameraRotationSpeedSlow = 0.5f;
        public static readonly float SecurityCameraRotationSpeedMedium = 
            2* SecurityCameraRotationSpeedSlow;
        public static readonly float SecurityCameraRotationSpeedFast =
            2 * SecurityCameraRotationSpeedMedium;

        public static readonly Vector3 SecurityCameraRotationAxisYaw = Vector3.UnitX;
        public static readonly Vector3 SecurityCameraRotationAxisPitch = Vector3.UnitY;
        public static readonly Vector3 SecurityCameraRotationAxisRoll = Vector3.UnitZ;
        #endregion

        #region Player
        public static readonly string PlayerOneID = "player1";
        public static readonly string PlayerTwoID = "player2";


        public static readonly Keys[] PlayerOneMoveKeys = { Keys.U, Keys.J, Keys.H, Keys.K, Keys.Y, Keys.I, Keys.N, Keys.M ,Keys.O ,Keys.L };
        public static readonly Keys[] PlayerTwoMoveKeys = { Keys.G, Keys.T, Keys.F, Keys.H, Keys.NumPad7, Keys.NumPad9, Keys.NumPad2, Keys.NumPad3 };
        public static readonly float PlayerMoveSpeed = 0.05f;
        public static readonly float PlayerStrafeSpeed = 0.7f * PlayerMoveSpeed;
        public static readonly float PlayerRotationSpeed = 0.08f;
        public static readonly float PlayerRadius = 3;
        public static readonly float PlayerHeight = 2;
        public static readonly float PlayerMass = 25;
        public static readonly float PlayerJumpHeight = 25;

        public static readonly float SquirrelPlayerMoveSpeed = 0;
        public static readonly float SquirrelPlayerRotationSpeed = 0.2f;
        public static readonly Keys[] SquirrelPlayerMoveKeys = { Keys.W, Keys.S, Keys.A, Keys.D, Keys.Space, Keys.R, Keys.F, Keys.G };

        public static readonly float DudeMoveSpeed = 0.15f;
        public static readonly float DudeRotationSpeed = 0.1f;
        public static readonly float DudeJumpHeight = 25;
        #endregion

        #region Menu
        public static readonly Keys KeyPauseShowMenu = Keys.Escape;
        public static readonly Keys KeyToggleCameraLayout = Keys.Up;
        #endregion

        #region Mouse
        //defines how much the mouse has to move in pixels before a movement is registered - see MouseManager::HasMoved()
        public static readonly float MouseSensitivity = 1;

        //always ensure that we start picking OUTSIDE the collidable first person camera radius - otherwise we will always pick ourself!
        public static readonly float PickStartDistance = CollidableCameraCapsuleRadius * 1.1f;
        public static readonly float PickEndDistance = 1000; //can be related to camera far clip plane radius but should be limited to typical level max diameter
        #endregion

        #region UI
        public static readonly string PlayerOneProgressID = PlayerOneID + " progress";
        public static readonly string PlayerOneScore = PlayerOneID + "Score: ";
        public static readonly string PlayerTwoProgressID = PlayerTwoID + " progress";
        public static readonly string PlayerOneProgressControllerID = PlayerOneProgressID + " ctrllr";
        public static readonly string PlayerTwoProgressControllerID = PlayerTwoProgressID + " ctrllr";
        #endregion

        #region Effect parameter ids used by the effect dictionary
        public static readonly string LitModelsEffectID = "lit models basic effect";
        public static readonly string UnlitModelsEffectID = "unlit models basic effect";
        public static readonly string UnLitPrimitivesEffectID = "unlit primitives basic effect";
        public static readonly string UnlitModelDualEffectID = "unlit models dual effect";
        public static readonly string UnlitBillboardsEffectID = "unlit billboards effect";


        #endregion

        #region Video
        public static readonly string VideoIDMainHall;
        public static readonly string ControllerIDSuffix = " controller";
        #endregion

        #region Primitive ids used by vertexData dictionary
        public static readonly string TexturedQuadID = "textured quad";
        public static readonly string TextStepID = "textured step";
        #endregion
    }
}
