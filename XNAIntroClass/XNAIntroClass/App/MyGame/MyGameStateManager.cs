﻿/* Use this class to say exactly how your game listens for events and responds with changes to the game.*/
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Managers.GDGame;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XNAIntroClass.App.MyGame
{
    public class MyGameStateManager : GameStateManager
    {
        public MyGameStateManager(Game game, EventDispatcher eventDispatcher, StatusType statusType)
            : base(game, eventDispatcher, statusType)
        {

        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            //to do...

            base.ApplyUpdate(gameTime);
        }
    }
}
