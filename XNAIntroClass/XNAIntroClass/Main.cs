﻿#define DEMO

using GDLibrary;
using GDLibrary.Actors.Camera;
using GDLibrary.Actors.Drawn._2D.UI;
using GDLibrary.Actors.Drawn._3D;
using GDLibrary.Actors.Drawn._3D.Collidable;
using GDLibrary.Actors.Drawn._3D.Collidable.Player.Animated;
using GDLibrary.Actors.Drawn._3D.Primitives;
using GDLibrary.Actors.Drawn2D.UI;
using GDLibrary.Controller._2D.Base;
using GDLibrary.Controller.Base;
using GDLibrary.Controller.Camera3D;
using GDLibrary.Controllers._2D.UI;
using GDLibrary.Controllers.Base;
using GDLibrary.Controllers.Camera3D;
using GDLibrary.Controllers.Camera3D.Collidable;
using GDLibrary.Controllers.Camera3D.Object;
using GDLibrary.Controllers.GDVideo;
using GDLibrary.Curve;
using GDLibrary.Debug;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Factory;
using GDLibrary.GDDebug.Physics;
using GDLibrary.Interfaces;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Content;
using GDLibrary.Managers.Input;
using GDLibrary.Managers.Object;
using GDLibrary.Managers.Physics;
using GDLibrary.Managers.Picking;
using GDLibrary.Managers.Screen;
using GDLibrary.Managers.Sound;
using GDLibrary.Managers.UI;
using GDLibrary.Parameters.Camera;
using GDLibrary.Parameters.Effects;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Primitives;
using GDLibrary.Parameters.Transforms;
using GDLibrary.Utility;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNAIntroClass.App.Actors;
using XNAIntroClass.Data;
using XNAIntroClass.Menu;

namespace XNAIntroClass
{
    public class Main : Microsoft.Xna.Framework.Game
    {
        #region Fields
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public ObjectManager objectManager { get; private set; }
        public CameraManager cameraManager { get; private set; }
        public MouseManager mouseManager { get; private set; }
        public KeyboardManager keyboardManager { get; private set; }
        public ScreenManager screenManager { get; private set; }
        public MyAppMenuManager menuManager { get; private set; }
        public PhysicsManager physicsManager { get; private set; }
        public UIManager uiManager { get; private set; }
        public GamePadManager gamePadManager { get; private set; }
        public SoundManager soundManager { get; private set; }
        public PickingManager pickingManager { get; private set; }
        private ManagerParameters managerParameters;

        //receives, handles and routes events
        public EventDispatcher eventDispatcher { get; private set; }

        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ContentDictionary<SpriteFont> fontDictionary;
        private ContentDictionary<Video> videoDictionary;

        //stores curves and rails used by cameras
        private Dictionary<string, Transform3DCurve> curveDictionary;
        private Dictionary<string, RailParameters> railDictionary;
        //stores viewport layouts for multi-screen layout
        private Dictionary<string, Viewport> viewPortDictionary;
        private Dictionary<string, EffectParameters> effectDictionary;
        private Dictionary<string, IVertexData> vertexDataDictionary;


        //player
        private HeroPlayerObject heroPlayerObject;
        private ModelObject drivableBoxObject;
        private AnimatedPlayerObject animatedHeroPlayerObject;
       

        private DebugDrawer debugDrawer;
        private PhysicsDebugDrawer physicsDebugDrawer;

        private int beatScore = 1;
        #endregion

        #region Constructor
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        #endregion

        #region Initialization
        protected override void Initialize()
        {
            //moved instanciation here to allow menu and ui managers to be moved to InitializeManagers()
            spriteBatch = new SpriteBatch(GraphicsDevice);

            int gameLevel = 1;
            bool isMouseVisible = true;
            Integer2 screenResolution = ScreenUtility.HD720;
            ScreenUtility.ScreenType screenType = ScreenUtility.ScreenType.SingleScreen;
            int numberOfGamePadPlayers = 1;
         

            //set the title
            Window.Title = "Beat hop the one stop spot for all this Bop";

            //initialize the Dispatcher
            InitializeEventDispatcher();

            //Load dictionaries, media assets and non-media assets
            LoadDictionaries();
            LoadAssets();
            LoadCurvesAndRails();
            LoadViewports(screenResolution);

            //to draw primitives and billboards
            LoadVertexData();

            //Initilize Effects
            InitializeEffects();

            //Initialize the managers
            InitializeManagers(screenResolution, screenType, isMouseVisible, numberOfGamePadPlayers);

            //menu and UI elements
            AddMenuElements();
            AddUIElements();
#if DEBUG
            //InitializeDebugTextInfo();
#endif

            //load game happens before cameras are loaded as we may need a third person camera that needs a 
            //reference to a loaded actor
            LoadGame(gameLevel);

            //Initialize cameras based in the desired screen layout
            if(screenType == ScreenUtility.ScreenType.SingleScreen)
            {
                //InitializeCollidableFirstPersonDemo(screenResolution);
                InitializeSingleScreenCycleableCameraDemo(screenResolution);
                //InitializeCollidableThirdPersonDemo(screenResolution);
            }
            else
            {
                InitializeMultiScreenCameraDemo(screenResolution);
            }

            //Publish Game Start event
            StartGame();

#if DEBUG
            //InitializeDebugCollisionSkinInfo();
#endif

            base.Initialize();
        }

        private void InitializeManagers(Integer2 screenResolution, ScreenUtility.ScreenType screenType, bool isMouseVisible,
            int numberOfGamePadPlayers) //1-4
        {
            //add sound manager
            this.soundManager = new SoundManager(this, this.eventDispatcher, StatusType.Update, "Content/Assets/Audio/", 
                "Demo2DSound.xgs", "WaveBank1.xwb", "SoundBank1.xsb");
            Components.Add(this.soundManager);

            this.cameraManager = new CameraManager(this, 1, this.eventDispatcher);
            Components.Add(this.cameraManager);

            this.objectManager = new ObjectManager(this, cameraManager, this.eventDispatcher, 10);
            //Components.Add(this.objectManager);

            this.keyboardManager = new KeyboardManager(this);
            Components.Add(this.keyboardManager);

            //create the manager which supports multiple camera viewports
            this.screenManager = new ScreenManager(this, graphics, screenResolution,
                screenType, this.objectManager, this.cameraManager,
                this.keyboardManager, AppData.KeyPauseShowMenu, this.eventDispatcher, StatusType.Off);
            Components.Add(this.screenManager);

            this.physicsManager = new PhysicsManager(this, this.eventDispatcher, StatusType.Off, AppData.BigGravity);
            Components.Add(this.physicsManager);

            this.mouseManager = new MouseManager(this, isMouseVisible, this.physicsManager);
            Components.Add(this.mouseManager);

            // add gamepad manager
            if (numberOfGamePadPlayers > 0)
            {
                this.gamePadManager = new GamePadManager(this, numberOfGamePadPlayers);
                Components.Add(this.gamePadManager);
            }

            //menu manager
            this.menuManager = new MyAppMenuManager(this, this.mouseManager, this.keyboardManager, this.cameraManager, 
                spriteBatch, this.eventDispatcher, StatusType.Off);
            //set the main menu to be the active menu scene
            this.menuManager.SetActiveList("mainmenu");
            Components.Add(this.menuManager);

            //ui (e.g. reticule, inventory, progress)
            this.uiManager = new UIManager(this, this.spriteBatch, this.eventDispatcher, 10, StatusType.Off);
            Components.Add(this.uiManager);

            //this object packages together all managers to give the mouse object the ability to listen for all forms of input from the user, as well as know where camera is etc.
            this.managerParameters = new ManagerParameters(this.objectManager,
                this.cameraManager, this.mouseManager, this.keyboardManager, this.gamePadManager, this.screenManager, this.soundManager);

            #region Pick Manager
            //call this function anytime we want to decide if a mouse over object is interesting to the PickingManager
            //See https://www.codeproject.com/Articles/114931/Understanding-Predicate-Delegates-in-C
            Predicate<CollidableObject> collisionPredicate = new Predicate<CollidableObject>(CollisionUtility.IsCollidableObjectOfInterest);
            //create the projectile archetype that the manager can fire

            //listens for picking with the mouse on valid (based on specified predicate) collidable objects and pushes notification events to listeners
            this.pickingManager = new PickingManager(this, this.eventDispatcher, StatusType.Off,
                this.managerParameters,
                PickingBehaviourType.PickAndRemove, AppData.PickStartDistance, AppData.PickEndDistance, collisionPredicate);
            Components.Add(this.pickingManager);
            #endregion

        }

        private void LoadDictionaries()
        {
            //models
            this.modelDictionary = new ContentDictionary<Model>("model dictionary", this.Content);

            //textures
            this.textureDictionary = new ContentDictionary<Texture2D>("texture dictionary", this.Content);

            //fonts
            this.fontDictionary = new ContentDictionary<SpriteFont>("font dictionary", this.Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            this.curveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails
            this.railDictionary = new Dictionary<string, RailParameters>();

            //viewports - used to store different viewports to be applied to multi-screen layouts
            this.viewPortDictionary = new Dictionary<string, Viewport>();

            //stores default effect parameters
            this.effectDictionary = new Dictionary<string, EffectParameters>();

            //notice we go back to using a content dictionary type since we want to pass strings and have dictionary load content
            this.videoDictionary = new ContentDictionary<Video>("video dictionary", this.Content);

            //used to store IVertexData (i.e. when we want to draw primitive objects, as in I-CA)
            this.vertexDataDictionary = new Dictionary<string, IVertexData>();
        }

        private void LoadAssets()
        {
            #region Models
            this.modelDictionary.Load("Assets/Models/plane1", "plane1");
            this.modelDictionary.Load("Assets/Models/box", "box");
            this.modelDictionary.Load("Assets/Models/box2", "box2");
            this.modelDictionary.Load("Assets/Models/torus", "torus");
            this.modelDictionary.Load("Assets/Models/sphere", "sphere");

            //triangle mesh for the high/medium and low poly versions
            this.modelDictionary.Load("Assets/Models/teapot");
            this.modelDictionary.Load("Assets/Models/teapot_mediumpoly");
            this.modelDictionary.Load("Assets/Models/teapot_lowpoly");

            //player - replace with animation eventually
            this.modelDictionary.Load("Assets/Models/cylinder");

            //architecture
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/house");
            this.modelDictionary.Load("Assets/Models/Architecture/Walls/wall");

            //dual texture demo
            this.modelDictionary.Load("Assets/Models/box");
            this.modelDictionary.Load("Assets/Models/box1");

            //BeatHop Models
            this.modelDictionary.Load("Assets/Models/buttonzone");
            this.modelDictionary.Load("Assets/Models/shelving");
            this.modelDictionary.Load("Assets/Models/littletable2");
            this.modelDictionary.Load("Assets/Models/tile");
            #endregion

            #region Textures
            //environment
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate1"); //demo use of the shorter form of Load() that generates key from asset name
            //this.textureDictionary.Load("Assets/Debug/Textures/checkerboard");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate2");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate3");
            this.textureDictionary.Load("Assets/Textures/Props/redmetal");
            this.textureDictionary.Load("Assets/Textures/Foliage/Ground/grass1");
            this.textureDictionary.Load("Assets/Textures/Skybox/back");
            this.textureDictionary.Load("Assets/Textures/Skybox/left");
            this.textureDictionary.Load("Assets/Textures/Skybox/right");
            this.textureDictionary.Load("Assets/Textures/Skybox/sky");
            this.textureDictionary.Load("Assets/Textures/Skybox/front");
            this.textureDictionary.Load("Assets/Textures/Foliage/Trees/tree2");
            this.textureDictionary.Load("Assets/Textures/checkerboard");
            this.textureDictionary.Load("Assets/Textures/checkerboard_greywhite");
            this.textureDictionary.Load("Assets/Textures/Landscape/beat_hop_ground");
            this.textureDictionary.Load("Assets/Textures/Landscape/subwall");

            //menu - buttons
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/genericbtn");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/genericbtn2");
            //menu - backgrounds
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/mainmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/audiomenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/controlsmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/exitmenuwithtrans");
            #endregion

            //ui (or hud) elements
            this.textureDictionary.Load("Assets/Textures/UI/HUD/reticuleDefault");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/scoreui");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/progress_gradient");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/menubackground");
            this.textureDictionary.Load("Assets/Textures/gamegoals");
            this.textureDictionary.Load("Assets/Textures/controls");

            //architecture
            this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/house-low-texture");
            this.textureDictionary.Load("Assets/Textures/Architecture/Walls/wall");

            //dual texture demo - see Main::InitializeCollidableGround()
           // this.textureDictionary.Load("Assets/Debug/Textures/checkerboard_greywhite");

            #region Fonts
            this.fontDictionary.Load("Assets/Debug/Fonts/debug");
            this.fontDictionary.Load("Assets/Fonts/menu");
            this.fontDictionary.Load("Assets/Fonts/mouse");
            #endregion

            #region Video
            this.videoDictionary.Load("Assets/Video/sample");
            this.videoDictionary.Load("Assets/Video/Wildlife");
            #endregion

            #region Animations
            //contains a single animation "Take001"
            this.modelDictionary.Load("Assets/Models/Animated/dude");

            //squirrel - one file per animation
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Idle");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Jump");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Punch");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Standing");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Tailwhip");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/RedRun4");
            #endregion
        }

        private void LoadCurvesAndRails()
        {
            int cameraHeight = 75;

            #region Curves
            //create the camera curve to be applied to the track controller
            Transform3DCurve curveA = new Transform3DCurve(CurveLoopType.Oscillate); //experiment with other CurveLoopTypes
            curveA.Add(new Vector3(0, cameraHeight, 300), -Vector3.UnitZ, Vector3.UnitY, 0); //start position
            curveA.Add(new Vector3(0, cameraHeight - 10, 175), -Vector3.UnitZ, Vector3.UnitY, 2);
            curveA.Add(new Vector3(0, 10, 0), -Vector3.UnitZ, Vector3.UnitY, 4);
            curveA.Add(new Vector3(0, 40, -240), -Vector3.UnitY, -Vector3.UnitZ, 8); //curve mid-point
            curveA.Add(new Vector3(0, 10, 0), -Vector3.UnitZ, Vector3.UnitY, 10);
            curveA.Add(new Vector3(0, cameraHeight - 10, 175), -Vector3.UnitZ, Vector3.UnitY, 12);
            curveA.Add(new Vector3(0, cameraHeight, 300), -Vector3.UnitZ, Vector3.UnitY, 16); //end position - same as start for zero-discontinuity on cycle
            //add to the dictionary
            this.curveDictionary.Add("unique curve name 1", curveA);
            #endregion

            #region Rails
            //create the track to be applied to the non-collidable track camera 1 EDITED RAIL PROPERTIESTO OVERHEAD CAM
            this.railDictionary.Add("rail1 - parallel to x-axis", new RailParameters("rail1 - parallel to x-axis", new Vector3(0, 50, 300), new Vector3(0, 70, -300)));
            //Rail for game Rules
            this.railDictionary.Add("rail2", new RailParameters("rail2", new Vector3(30, 15, 240), new Vector3(30, 15, -300)));
            #endregion
        }

        private void LoadVertexData()
        {
            Microsoft.Xna.Framework.Graphics.PrimitiveType primitiveType;
            int primitiveCount;
            IVertexData vertexData = null;

            #region Textured Quad
            //get vertices for textured quad
            VertexPositionColorTexture[] vertices = VertexFactory.GetTextureQuadVertices(out primitiveType, out primitiveCount);

            //make a vertex data object to store and draw the vertices
            vertexData = new BufferedVertexData<VertexPositionColorTexture>(this.graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            //add to the dictionary for use by things like billboards - see InitializeBillboards()
            this.vertexDataDictionary.Add(AppData.TexturedQuadID, vertexData);
            #endregion
            // VEC BOX
            VertexPositionColorTexture[] PyramidSquare = VertexFactory.GetVerticesPositionTexturedPyramidSquare(200, out primitiveType, out primitiveCount);
            //make a vertex data object to store and draw the vertices
            vertexData = new BufferedVertexData<VertexPositionColorTexture>(this.graphics.GraphicsDevice, PyramidSquare, primitiveType, primitiveCount);
           
            // add to the dictionary
            this.vertexDataDictionary.Add(AppData.TextStepID, vertexData);

            //#region Billboard Quad - we must use this type when creating billboards
            //// get vertices for textured billboard
            //VertexBillboard[] verticesBillboard = VertexFactory.GetVertexBillboard(1, out primitiveType, out primitiveCount);

            ////make a vertex data object to store and draw the vertices
            //vertexData = new BufferedVertexData<VertexBillboard>(this.graphics.GraphicsDevice, verticesBillboard, primitiveType, primitiveCount);

            ////add to the dictionary for use by things like billboards - see InitializeBillboards()
            //this.vertexDataDictionary.Add(AppData.TexturedBillboardQuadID, vertexData);
            //#endregion

        }

        private void LoadViewports(Integer2 screenResolution)
        {
           
            //the full screen viewport with optional padding
            int leftPadding = 0, topPadding = 0, rightPadding = 0, bottomPadding = 0;
            Viewport paddedFullViewPort = ScreenUtility.Pad(new Viewport(0, 0, screenResolution.X, (int)(screenResolution.Y)), leftPadding, topPadding, rightPadding, bottomPadding);
            this.viewPortDictionary.Add("full viewport", paddedFullViewPort);

            //work out the dimensions of the small camera views along the left hand side of the screen
            int smallViewPortHeight = 144; //6 small cameras along the left hand side of the main camera view i.e. total height / 5 = 720 / 5 = 144
            int smallViewPortWidth = 5 * smallViewPortHeight / 3; //we should try to maintain same ProjectionParameters aspect ratio for small cameras as the large     
            //the five side viewports in multi-screen mode
            this.viewPortDictionary.Add("column0 row0", new Viewport(0, 0, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row1", new Viewport(0, 1 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row2", new Viewport(0, 2 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row3", new Viewport(0, 3 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row4", new Viewport(0, 4 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            //the larger view to the right in column 1
            this.viewPortDictionary.Add("column1 row0", new Viewport(smallViewPortWidth, 0, screenResolution.X - smallViewPortWidth, screenResolution.Y));
        }

#if DEBUG
        private void InitializeDebugTextInfo()
        {
            //add debug info in top left hand corner of the screen
            this.debugDrawer = new DebugDrawer(this, this.screenManager, this.cameraManager, this.objectManager, spriteBatch,
                this.fontDictionary["debug"], Color.White, new Vector2(5, 5), this.eventDispatcher, StatusType.Off);
            Components.Add(this.debugDrawer);

        }

        private void InitializeDebugCollisionSkinInfo()
        {
            //show the collision skins
            this.physicsDebugDrawer = new PhysicsDebugDrawer(this, this.cameraManager, this.objectManager,
                this.screenManager, this.eventDispatcher, StatusType.Off);
            Components.Add(this.physicsDebugDrawer);
        }
#endif
        #endregion

        #region Load Game Content
        //load the contents for the level specified
        private void LoadGame(int level)
        {
            int worldScale = 250;

            //Non-collidable
            InitializeNonCollidableSkyBox(worldScale);
            //InitializeNonCollidableFoliage(worldScale);
            ///InitializeNonCollidableDriveableObject();
            //InitializeNonCollidableDecoratorObjects();

            //Collidable
            InitializeCollidableGround(worldScale);
            //demo high vertex count trianglemesh
            //InitializeStaticCollidableTriangleMeshObjects();
            //demo medium and low vertex count trianglemesh
            //InitializeStaticCollidableMediumPolyTriangleMeshObjects();
            //InitializeStaticCollidableLowPolyTriangleMeshObjects();
            //demo dynamic collidable objects with user-defined collision primitives
            //InitializeDynamicCollidableObjects();

            //InitializeNonCollidableDriveableObject();
            //adds the hero of the game - see InitializeSingleScreenFirstThirdPersonDemo()
            //InitializeCollidableHeroPlayerObject();
            ////add level elements
            //InitializeBuildings();
            //InitializeWallsFences();
            InitializeBeatBox();

            //add video display
            InitializeVideoDisplay();

            //add animated characters
            //bool bTheDudeAbides = true;
            //if (bTheDudeAbides)
            //   InitializeDudeAnimatedPlayer();
            //else
            //    InitializeSquirrelAnimatedPlayer();

            ////add primitive objects - where developer defines the vertices manually
            InitializePrimitives();
            InitializeCollidableHeroPlayerObject();
            InitializeZoneBox();
            InitializeTrophy();
            InitializeControlsBox();
            InitializeLevelWalls();
            //InitializeVerticesBox();

        }

        private void InitializeControlsBox()
        {
            Transform3D transform = new Transform3D(new Vector3(50, 0, 300), Vector3.Zero, new Vector3(15,10,15), Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["controls"];
            effectParameters.Alpha = 0.9f;

            //initialise the boxObject
            ModelObject boxObject = new ModelObject("control box 1", ActorType.Decorator, transform, effectParameters, this.modelDictionary["box2"]);
            //add to the objectManager so that it will be drawn and updated
            this.objectManager.Add(boxObject);

            ModelObject clonedObject = null;

            for (int i = 1; i <= 1; i++)
            {
                clonedObject = (ModelObject)boxObject.Clone();

                clonedObject.Transform3D.Translation = new Vector3(80, 9, -20);


                //add to manager
                this.objectManager.Add(clonedObject);
            }

        }

        private void InitializeLevelWalls()
        {
            CollidableObject clonedObject = null;
            Transform3D transform3D = new Transform3D(new Vector3(-25f, 0, -295), new Vector3(0,90,0), new Vector3(6,1,20), Vector3.UnitX, Vector3.UnitY);
            //clone the dictionary effect and set unique properties for the hero player object

            // Model Effects
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnlitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["subwall"];
            effectParameters.DiffuseColor = Color.White;
            effectParameters.SpecularPower = 32; //pow((N, H), SpecularPower)
            effectParameters.EmissiveColor = Color.Aquamarine;

            //Load Model into Engine
            CollidableObject archetypeCollidableObject = new TriangleMeshObject("tile", ActorType.CollidableProp, transform3D, effectParameters,
                            this.modelDictionary["tile"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            archetypeCollidableObject.Enable(true, 1);
            this.objectManager.Add(archetypeCollidableObject);

            /*
            for (int i = 1; i <= 4; i++)
            {
                clonedObject = (CollidableObject)archetypeCollidableObject.Clone();
                clonedObject.ID += 1;

                clonedObject.Transform3D = new Transform3D(new Vector3(25 + 5 * i, 15 + 10 * i, 0), new Vector3(0, 0, 0), new Vector3(2, 4, 1), Vector3.UnitX, Vector3.UnitY);

                //clonedObject.AddPrimitive(new Box(clonedObject.Transform3D.Translation, Matrix.Identity, /*important do not change - cm to inch*/
            //2.54f * clonedObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));

            /*
               clonedObject.Enable(true, 1);

               this.objectManager.Add(clonedObject);
           }*/
        }
        private void InitializeZoneBox()
        {
            PrimitiveObject clonedObject = null;

            //Object point perameters
            VertexPositionColor[] vertices = new VertexPositionColor[2];
            vertices[0] = new VertexPositionColor(-Vector3.UnitX, Color.Black);
            vertices[1] = new VertexPositionColor(Vector3.UnitX, Color.Bisque);
            IVertexData zoneVertexData = new VertexData<VertexPositionColor>(vertices, Microsoft.Xna.Framework.Graphics.PrimitiveType.LineStrip, 1);

            //Location
            Transform3D transform = new Transform3D(new Vector3(0, 10, -5),new Vector3(5,1,1) );
            //Effect
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitPrimitivesEffectID].Clone() as BasicEffectParameters;
            //Loading Object
            PrimitiveObject thePrimitive = new PrimitiveObject("line", ActorType.Primitive,
               transform, effectParameters, StatusType.Drawn | StatusType.Update, zoneVertexData);

            this.objectManager.Add(thePrimitive);

            for(int i = 1; i <= 4; i++)
            {
                clonedObject = thePrimitive.Clone() as PrimitiveObject;

                clonedObject.Transform3D.Translation += new Vector3(0, 5 * i, 0);

                //we could also attach controllers here instead to give each a different rotation
                clonedObject.AttachController(new RotationController("rot controller", ControllerType.Rotation, new Vector3(0.1f * i, 0, 0)));

                //add to manager
                this.objectManager.Add(clonedObject);
            }
        }
        private void InitializeTrophy()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-32.5f, 0, -5), new Vector3(0, 0, 0), new Vector3(1, 1, 1), Vector3.UnitX, Vector3.UnitY);
            //clone the dictionary effect and set unique properties for the hero player object

            // Model Effects
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnlitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["subwall"];
            effectParameters.DiffuseColor = Color.White;
            effectParameters.SpecularPower = 32; //pow((N, H), SpecularPower)
            effectParameters.EmissiveColor = Color.Aquamarine;

            //Load Model into Engine
            CollidableObject archetypeCollidableObject = new TriangleMeshObject("tile", ActorType.CollidableProp, transform3D, effectParameters,
                            this.modelDictionary["tile"], new MaterialProperties(0.2f, 0.8f, 0.7f));
        }

        private void InitializeVerticesBox()
        {
            Transform3D transform = new Transform3D(new Vector3(5, 5, 5), Vector3.Zero, Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            PrimitiveObject primitiveObject = new PrimitiveObject("simple primitive", ActorType.Primitive,
               transform, effectParameters, StatusType.Drawn | StatusType.Update, this.vertexDataDictionary[AppData.TextStepID]);

        }
        private void InitializePrimitives()
        {
            //get a copy of the effect parameters
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitPrimitivesEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            effectParameters.DiffuseColor = Color.Yellow;
            effectParameters.Alpha = 0.4f;

            //define location
            Transform3D transform = new Transform3D(new Vector3(0, 40, 0), new Vector3(40, 4, 1));

            //create primitive
            PrimitiveObject primitiveObject = new PrimitiveObject("simple primitive", ActorType.Primitive,
                transform, effectParameters, StatusType.Drawn | StatusType.Update, this.vertexDataDictionary[AppData.TexturedQuadID]);

            PrimitiveObject clonedPrimitiveObject = null;

            for (int i = 1; i <= 4; i++)
            {
                clonedPrimitiveObject = primitiveObject.Clone() as PrimitiveObject;
                clonedPrimitiveObject.Transform3D.Translation += new Vector3(0, 5 * i, 0);

                //we could also attach controllers here instead to give each a different rotation
                clonedPrimitiveObject.AttachController(new RotationController("rot controller", ControllerType.Rotation, new Vector3(0.1f * i, 0, 0)));

                //add to manager
                this.objectManager.Add(clonedPrimitiveObject);
            }

        }

        private void InitializeSquirrelAnimatedPlayer()
        {
            Transform3D transform3D = null;

            transform3D = new Transform3D(new Vector3(0, 20, 30),
                new Vector3(-90, 0, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                 2f * Vector3.One,
                 Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.DiffuseColor = Color.OrangeRed;
            //no specular, emissive, directional lights
            //if we dont specify a texture then the object manager will draw using whatever textures were baked into the animation in 3DS Max
            effectParameters.Texture = this.textureDictionary["checkerboard_greywhite"];

            this.animatedHeroPlayerObject = new SquirrelAnimatedPlayerObject("squirrel",
            ActorType.Player, transform3D,
                effectParameters,
                AppData.PlayerOneMoveKeys,
                AppData.PlayerRadius,
                AppData.PlayerHeight,
                1, 1,  //accel, decel
                AppData.SquirrelPlayerMoveSpeed,
                AppData.SquirrelPlayerRotationSpeed,
                AppData.PlayerJumpHeight,
                new Vector3(0, 0, 0), //offset inside capsule
                this.keyboardManager);
            this.animatedHeroPlayerObject.Enable(false, AppData.PlayerMass);

            //add the animations
            string takeName = "Take 001";
            string fileNameNoSuffix = "Red_Idle";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Jump";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Punch";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Standing";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Tailwhip";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "RedRun4";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);

            //set the start animtion
            this.animatedHeroPlayerObject.SetAnimation("Take 001", "Red_Idle");

            this.objectManager.Add(animatedHeroPlayerObject);

        }

        private void InitializeCollidableHeroPlayerObject()
        {
            Transform3D transform = new Transform3D(new Vector3(-0.5f, 1, 303),
                new Vector3(90, 0, 0),
                new Vector3(2, 2, 2), -Vector3.UnitZ, Vector3.UnitY);

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate3"];

            //make the hero a field since we need to point the third person camera controller at this object
            this.heroPlayerObject = new HeroPlayerObject(AppData.PlayerOneID,
                AppData.PlayerOneProgressControllerID, //used to increment/decrement progress on pickup, win, or lose
                ActorType.Player, transform, effectParameters,
                this.modelDictionary["box2"],
                AppData.PlayerTwoMoveKeys,
                AppData.PlayerRadius, AppData.PlayerHeight,
                1f, 1f,
                AppData.PlayerJumpHeight,
                new Vector3(0, 5, 0),
                this.keyboardManager);
            this.heroPlayerObject.Enable(false, AppData.PlayerMass);

            //don't forget to add it - or else we wont see it!
            this.objectManager.Add(this.heroPlayerObject);
        }

        //skybox is a non-collidable series of ModelObjects with no lighting
        private void InitializeNonCollidableSkyBox(int worldScale)
        {
            //first we will create a prototype plane and then simply clone it for each of the skybox decorator elements (e.g. ground, front, top etc). 
            Transform3D transform = new Transform3D(new Vector3(0, -5, 0), new Vector3(worldScale, 1, worldScale));

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnlitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            ModelObject planePrototypeModelObject = new ModelObject("plane1", ActorType.Decorator, transform, 
                effectParameters,
                this.modelDictionary["plane1"]);

            //will be re-used for all planes
            ModelObject clonePlane = null;

            #region Skybox
            //we no longer add a simple grass plane - see InitializeCollidableGround()
            //clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            //clonePlane.Texture = this.textureDictionary["grass1"];
            //this.objectManager.Add(clonePlane);

            //add the back skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["back"];
            //rotate the default plane 90 degrees around the X-axis (use the thumb and curled fingers of your right hand to determine +ve or -ve rotation value)
            clonePlane.Transform3D.Rotation = new Vector3(90, 0, 0);
            /*
             * Move the plane back to meet with the back edge of the grass (by based on the original 3DS Max model scale)
             * Note:
             * - the interaction between 3DS Max and XNA units which result in the scale factor used below (i.e. 1 x 2.54 x worldScale)/2
             * - that I move the plane down a little on the Y-axiz, purely for aesthetic purposes
             */
            clonePlane.Transform3D.Translation = new Vector3(0, -5, (-2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);

            //As an exercise the student should add the remaining 4 skybox planes here by repeating the clone, texture assignment, rotation, and translation steps above...
            //add the left skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["left"];
            clonePlane.Transform3D.Rotation = new Vector3(90, 90, 0);
            clonePlane.Transform3D.Translation = new Vector3((-2.54f * worldScale) / 2.0f, -5, 0);
            this.objectManager.Add(clonePlane);

            //add the right skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["right"];
            clonePlane.Transform3D.Rotation = new Vector3(90, -90, 0);
            clonePlane.Transform3D.Translation = new Vector3((2.54f * worldScale) / 2.0f, -5, 0);
            this.objectManager.Add(clonePlane);

            //add the top skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["sky"];
            //notice the combination of rotations to correctly align the sky texture with the sides
            clonePlane.Transform3D.Rotation = new Vector3(180, -90, 0);
            clonePlane.Transform3D.Translation = new Vector3(0, ((2.54f * worldScale) / 2.0f) - 5, 0);
            this.objectManager.Add(clonePlane);

            //add the front skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["front"];
            clonePlane.Transform3D.Rotation = new Vector3(-90, 0, 180);
            clonePlane.Transform3D.Translation = new Vector3(0, -5, (2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);
            #endregion
        }

        private void InitializeNonCollidableFoliage(int worldScale)
        {
            //first we will create a prototype plane and then simply clone it for each of the decorator elements (e.g. trees etc). 
            Transform3D transform = new Transform3D(new Vector3(0, -5, 0), new Vector3(worldScale, 1, worldScale));

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            //a fix to ensure that any image containing transparent pixels will be sent to the correct draw list in ObjectManager
            effectParameters.Alpha = 0.99f;

            ModelObject planePrototypeModelObject = new ModelObject("plane1", ActorType.Decorator, transform, effectParameters,
                this.modelDictionary["plane1"]);

            //will be re-used for all planes
            ModelObject clonePlane = null;

            #region Add Trees
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["tree2"];
            clonePlane.Transform3D.Rotation = new Vector3(90, 0, 0);
            /*
             * ISRoT - Scale operations are applied before rotation in XNA so to make the tree tall (i.e. 10) we actually scale 
             * along the Z-axis (remember the original plane is flat on the XZ axis) and then flip the plane to stand upright.
             */
            clonePlane.Transform3D.Scale = new Vector3(5, 1, 10);
            //y-displacement is (10(XNA) x 2.54f(3DS Max))/2 - 5(ground level) = 7.7f
            clonePlane.Transform3D.Translation = new Vector3(0, ((clonePlane.Transform3D.Scale.Z * 2.54f) / 2 - 5), -20);
            this.objectManager.Add(clonePlane);
            #endregion
        }

        //the ground is just a large flat box - use a Box collision primitive
        private void InitializeCollidableGround(int worldScale)
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;

            /*
             * Note that if we use DualTextureEffectParameters then (a) we must create a model (i.e. box2.fbx) in 3DS Max with two texture channels (i.e. use Unwrap UVW twice)
             * because each texture (diffuse and lightmap) requires a separate set of UV texture coordinates, and (b), this effect does NOT allow us to set up lighting. 
             * Why? Well, we don't need lighting because we can bake a static lighting response into the second texture (the lightmap) in 3DS Max).
             * 
             * See https://knowledge.autodesk.com/support/3ds-max/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/3DSMax/files/GUID-37414F9F-5E33-4B1C-A77F-547D0B6F511A-htm.html
             * See https://www.youtube.com/watch?v=vuHdnxkXpYo&t=453s
             * See https://www.youtube.com/watch?v=AqiNpRmENIQ&t=1892s
             * 
             */
            Model model = this.modelDictionary["box2"];
            //a simple dual texture demo - dual textures can be used with a lightMap from 3DS Max using the Render to Texture setting
            //BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            DualTextureEffectParameters effectParameters = this.effectDictionary[AppData.UnlitModelDualEffectID].Clone() as DualTextureEffectParameters;
            effectParameters.Texture = this.textureDictionary["beat_hop_ground"];
            effectParameters.Texture2 = this.textureDictionary["checkerboard_greywhite"];

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(worldScale, 0.001f, worldScale), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new CollidableObject("ground", ActorType.CollidableGround, transform3D, effectParameters, model);
            collidableObject.AddPrimitive(new JigLibX.Geometry.Plane(transform3D.Up, transform3D.Translation), new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false, see what happens.
            this.objectManager.Add(collidableObject);
        }

        //Triangle mesh objects wrap a tight collision surface around complex shapes - the downside is that TriangleMeshObjects CANNOT be moved
        private void InitializeStaticCollidableTriangleMeshObjects()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-50, 10, 0), new Vector3(45, 45, 0), 0.1f * Vector3.One, Vector3.UnitX, Vector3.UnitY);
            //clone the dictionary effect and set unique properties for the hero player object

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            effectParameters.DiffuseColor = Color.White;
            effectParameters.SpecularPower = 32; //pow((N, H), SpecularPower)
            effectParameters.EmissiveColor = Color.Aquamarine;
            

            CollidableObject collidableObject = new TriangleMeshObject("torus", ActorType.CollidableProp, transform3D, effectParameters,
                            this.modelDictionary["torus"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        //Demo use of a low-poly model to generate the triangle meshh collision skin
        //This is much more efficient than using a full mesh on the model
        private void InitializeStaticCollidableMediumPolyTriangleMeshObjects()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-30, 3, 0),
                new Vector3(0, 0, 0), 0.08f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            CollidableObject collidableObject = new TriangleMeshObject("teapot", ActorType.CollidableProp, transform3D, effectParameters,
                        this.modelDictionary["teapot"], this.modelDictionary["teapot_mediumpoly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        //Demos use of a low-polygon model to generate the triangle mesh collision skin - saving CPU cycles on CDCR checking
        private void InitializeStaticCollidableLowPolyTriangleMeshObjects()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-10, 3, 0),
                new Vector3(0, 0, 0), 0.08f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            //lets set the diffuse color also, for fun.
            effectParameters.DiffuseColor = Color.Blue;

            CollidableObject collidableObject = new TriangleMeshObject("teapot", ActorType.CollidableProp,
            transform3D, effectParameters, this.modelDictionary["teapot"],
            this.modelDictionary["teapot_lowpoly"],
               new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        //if you want objects to be collidable AND moveable then you must attach either a box, sphere, or capsule primitives to the object
        private void InitializeDynamicCollidableObjects()
        {
            CollidableObject collidableObject, archetypeCollidableObject = null;
            Model model = null;

            #region Spheres
            model = this.modelDictionary["sphere"];
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            //make once then clone
            archetypeCollidableObject = new CollidableObject("sphere ", ActorType.CollidablePickup, Transform3D.Zero, effectParameters, model);

            for (int i = 0; i < 10; i++)
            {
                collidableObject = (CollidableObject)archetypeCollidableObject.Clone();

                collidableObject.ID += i;
                collidableObject.Transform3D = new Transform3D(new Vector3(-50, 100 + 10 * i, i), new Vector3(0, 0, 0),
                    0.082f * Vector3.One, //notice theres a certain amount of tweaking the radii with reference to the collision sphere radius of 2.54f below
                    Vector3.UnitX, Vector3.UnitY);

                collidableObject.AddPrimitive(new Sphere(collidableObject.Transform3D.Translation, 2.54f), new MaterialProperties(0.2f, 0.8f, 0.7f));
                collidableObject.Enable(false, 1);
                this.objectManager.Add(collidableObject);
            }
            #endregion

            #region Box
            model = this.modelDictionary["box2"];
            effectParameters = (this.effectDictionary[AppData.LitModelsEffectID] as BasicEffectParameters).Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate2"];
            //make once then clone
            archetypeCollidableObject = new CollidableObject("box - ", ActorType.CollidablePickup, Transform3D.Zero, effectParameters, model);

            int count = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    collidableObject = (CollidableObject)archetypeCollidableObject.Clone();
                    collidableObject.ID += count;
                    count++;

                    collidableObject.Transform3D = new Transform3D(new Vector3(25 + 5 * j, 15 + 10 * i, 0), new Vector3(0, 0, 0), new Vector3(2, 4, 1), Vector3.UnitX, Vector3.UnitY);
                    collidableObject.AddPrimitive(new Box(collidableObject.Transform3D.Translation, Matrix.Identity, /*important do not change - cm to inch*/2.54f * collidableObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));

                    //increase the mass of the boxes in the demo to see how collidable first person camera interacts vs. spheres (at mass = 1)
                    collidableObject.Enable(false, 1);
                    this.objectManager.Add(collidableObject);
                }
            }

            #endregion
        }

        //demo of non-collidable ModelObject with attached third person controller
        private void InitializeNonCollidableDriveableObject()
        {
            //place the drivable model to the left of the existing models and specify that forward movement is along the -ve z-axis
            Transform3D transform = new Transform3D(new Vector3(-1, 3, 311), -Vector3.UnitZ, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate3"];
            effectParameters.DiffuseColor = Color.Gold;

            //initialise the drivable model object - we've made this variable a field to allow it to be visible to the rail camera controller - see InitializeCameras()
            this.drivableBoxObject = new ModelObject("drivable box1", ActorType.Player, transform, effectParameters,
                this.modelDictionary["box2"]);
            

            //Makes box collidable
           CollidableObject collidableObject = new TriangleMeshObject("drivable box3", ActorType.CollidableProp,
           this.drivableBoxObject.Transform3D, effectParameters, this.modelDictionary["box2"],
           this.modelDictionary["box2"],
               new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject); 

            //new CollDetectBoxbox = 

            //attach a DriveController
            drivableBoxObject.AttachController(new DriveController("driveController1", ControllerType.Drive,
                AppData.PlayerOneMoveKeys, AppData.PlayerMoveSpeed, AppData.PlayerStrafeSpeed, AppData.PlayerRotationSpeed, this.managerParameters));
            //drivableBoxObject.BoundingSphere(new CollidableObject("HitBox", ActorType.CollidableProp, this.drivableBoxObject.Transform3D, effectParameters, this.modelDictionary["box2"]));
            //add to the objectManager so that it will be drawn and updated
            this.objectManager.Add(drivableBoxObject);
        }

        private void InitializeBeatBox()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-10, -25, 305),new Vector3(-0, -15, -15), 0.05f * Vector3.One, Vector3.UnitX, Vector3.UnitY);
            //clone the dictionary effect and set unique properties for the hero player object

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["redmetal"];
            effectParameters.DiffuseColor = Color.White;
            effectParameters.SpecularPower = 32; //pow((N, H), SpecularPower)
            effectParameters.EmissiveColor = Color.Aquamarine;
            effectParameters.Alpha = 0.5f;


            CollidableObject archetypeCollidableObject = new TriangleMeshObject("box", ActorType.CollidableProp, transform3D, effectParameters,
                            this.modelDictionary["buttonzone"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            archetypeCollidableObject.Enable(false, 1);
            this.objectManager.Add(archetypeCollidableObject);

            //a clone variable that we can reuse
            CollidableObject collidableObject = null;
            

            //Lane Spawn Pattern
            //Lane 1
            for (int i = 0; i < 4; i++)
            {
                collidableObject = (CollidableObject)archetypeCollidableObject.Clone();

                collidableObject.ID += i;
                collidableObject.Transform3D = new Transform3D(new Vector3(-18.5f, 4, 300 -(200 * i)), new Vector3(0, 0, 0),
                    0.082f * Vector3.One, //notice theres a certain amount of tweaking the radii with reference to the collision sphere radius of 2.54f below
                    Vector3.UnitX, Vector3.UnitY);

                collidableObject.AddPrimitive(new Sphere(collidableObject.Transform3D.Translation, 2.54f), new MaterialProperties(0.2f, 0.8f, 0.7f));
                collidableObject.Enable(false, 1);
                collidableObject.Mass = 0.3f;
                this.objectManager.Add(collidableObject);
            }
            //Lane 2
            for (int i = 0; i < 4; i++)
            {
                collidableObject = (CollidableObject)archetypeCollidableObject.Clone();

                collidableObject.ID += i;
                collidableObject.Transform3D = new Transform3D(new Vector3(-9.25f, 4, 240 - (125 * i)), new Vector3(0, 0, 0),
                    0.082f * Vector3.One, //notice theres a certain amount of tweaking the radii with reference to the collision sphere radius of 2.54f below
                    Vector3.UnitX, Vector3.UnitY);

                collidableObject.AddPrimitive(new Sphere(collidableObject.Transform3D.Translation, 2.54f), new MaterialProperties(0.2f, 0.8f, 0.7f));
                collidableObject.Enable(false, 1);
                collidableObject.Mass = 0.3f;
                this.objectManager.Add(collidableObject);
            }
            //Lane 3
            for (int i = 0; i < 4; i++)
            {
                collidableObject = (CollidableObject)archetypeCollidableObject.Clone();

                collidableObject.ID += i;
                collidableObject.Transform3D = new Transform3D(new Vector3(0, 4, 280 - (200 * i)), new Vector3(0, 0, 0),
                    0.082f * Vector3.One, //notice theres a certain amount of tweaking the radii with reference to the collision sphere radius of 2.54f below
                    Vector3.UnitX, Vector3.UnitY);

                collidableObject.AddPrimitive(new Sphere(collidableObject.Transform3D.Translation, 2.54f), new MaterialProperties(0.2f, 0.8f, 0.7f));
                collidableObject.Enable(false, 1);
                collidableObject.Mass = 0.3f;
                this.objectManager.Add(collidableObject);
            }
            //Lane 4
            for (int i = 0; i < 4; i++)
            {
                collidableObject = (CollidableObject)archetypeCollidableObject.Clone();

                collidableObject.ID += i;
                collidableObject.Transform3D = new Transform3D(new Vector3(9f, 4, 260 - (75 * i)), new Vector3(0, 0, 0),
                    0.082f * Vector3.One, //notice theres a certain amount of tweaking the radii with reference to the collision sphere radius of 2.54f below
                    Vector3.UnitX, Vector3.UnitY);

                collidableObject.AddPrimitive(new Sphere(collidableObject.Transform3D.Translation, 2.54f), new MaterialProperties(0.2f, 0.8f, 0.7f));
                collidableObject.Enable(false, 1);
                collidableObject.Mass = 0.3f;
                this.objectManager.Add(collidableObject);
            }
            //Lane 5
            for (int i = 0; i < 4; i++)
            {
                collidableObject = (CollidableObject)archetypeCollidableObject.Clone();

                collidableObject.ID += i;
                collidableObject.Transform3D = new Transform3D(new Vector3(18, 4, 200 - (45 * i)), new Vector3(0, 0, 0),
                    0.082f * Vector3.One, //notice theres a certain amount of tweaking the radii with reference to the collision sphere radius of 2.54f below
                    Vector3.UnitX, Vector3.UnitY);

                collidableObject.AddPrimitive(new Sphere(collidableObject.Transform3D.Translation, 2.54f), new MaterialProperties(0.2f, 0.8f, 0.7f));
                collidableObject.Enable(false, 1);
                collidableObject.Mass = 0.3f;
                this.objectManager.Add(collidableObject);
            }

        }

        //demo of some semi-transparent non-collidable ModelObjects
        private void InitializeNonCollidableDecoratorObjects()
        {
            //use one of our static defaults to position the object at the origin
            Transform3D transform = new Transform3D(new Vector3(0,5,0), Vector3.Zero, Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate1"];
            effectParameters.DiffuseColor = Color.Gold;
            effectParameters.Alpha = 0.5f;

            //initialise the boxObject
            ModelObject boxObject = new ModelObject("some box 1", ActorType.Decorator, transform, effectParameters, this.modelDictionary["box2"]);
            //add to the objectManager so that it will be drawn and updated
            this.objectManager.Add(boxObject);

            //a clone variable that we can reuse
            ModelObject clone = null;

            //add a clone of the box model object to test the clone
            clone = (ModelObject)boxObject.Clone();
            clone.Transform3D.Translation = new Vector3(5, 0, 0);
            //scale it to make it look different
            clone.Transform3D.Scale = new Vector3(1, 4, 1);
            //change its color
            clone.EffectParameters.DiffuseColor = Color.Red;
            this.objectManager.Add(clone);

            //add more clones here...
        }

        private void InitializeBuildings()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-100, 0, 0),
                new Vector3(0, 90, 0), 0.4f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["house-low-texture"];

            CollidableObject collidableObject = new TriangleMeshObject("house1", ActorType.CollidableArchitecture, transform3D,
                                effectParameters, this.modelDictionary["house"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        private void InitializeWallsFences()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-140, 0, -14),
                new Vector3(0, -90, 0), 0.4f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["wall"];

            CollidableObject collidableObject = new TriangleMeshObject("wall1", ActorType.CollidableArchitecture, transform3D,
                            effectParameters, this.modelDictionary["wall"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        private void InitializeVideoDisplay()
        {

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["gamegoals"];
            //make the screen really shiny
            effectParameters.SpecularPower = 200;

            //put the display up on the Y-axis, obviously we can rotate by setting transform3D.Rotation
            Transform3D transform3D = new Transform3D(new Vector3(40, 20, 50),new Vector3(14, 16, 0.01f));

            /* 
             * Does the display need to be collidable? if so use a CollidableObject and not a ModelObject.
             * Notice we dont pass in a texture since the video controller will set this.
             */
            ModelObject videoModelObject = new ModelObject(AppData.VideoIDMainHall, ActorType.Decorator,
                transform3D, effectParameters, this.modelDictionary["box"]);

            ////create the controller
            VideoController videoController = new VideoController(AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, ControllerType.Video, this.eventDispatcher,
                this.textureDictionary["gamegoals"], this.videoDictionary["Wildlife"], 0.5f);

            //make it rotate like a commercial video display
            videoModelObject.AttachController(new RotationController(AppData.VideoIDMainHall + " rotation " + AppData.ControllerIDSuffix,
                ControllerType.Rotation, new Vector3(-0.001f, 0.005f, 0)));
            videoModelObject.AttachController(new RailController("VideoRadioID", ControllerType.Rail, heroPlayerObject, this.railDictionary["rail2"]));
            //attach
            videoModelObject.AttachController(videoController);
            //add to object manager
            this.objectManager.Add(videoModelObject);
        }

        #endregion

        #region Initialize Cameras
        private void InitializeCamera(Integer2 screenResolution, string id, Viewport viewPort, Transform3D transform, 
            IController controller, float drawDepth)
        {
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform, ProjectionParameters.StandardMediumFiveThree, viewPort, 1, StatusType.Update);

            if (controller != null)
                camera.AttachController(controller);

            this.cameraManager.Add(camera);
        }

        private void InitializeMultiScreenCameraDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "";
            int cameraHeight = 100;

            //non-collidable camera 1
            id = "non-collidable FPC 1";
            viewportDictionaryKey = "column1 row0";
            transform = new Transform3D(new Vector3(0, cameraHeight, 10), -Vector3.UnitZ, Vector3.UnitY);
            controller = new FirstPersonCameraController(id + " controller", ControllerType.FirstPerson, AppData.CameraMoveKeys, AppData.CameraMoveSpeed, AppData.CameraStrafeSpeed, AppData.CameraRotationSpeed,
                this.managerParameters);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 1
            id = "non-collidable security 1";
            viewportDictionaryKey = "column0 row0";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 60, AppData.SecurityCameraRotationSpeedSlow, AppData.SecurityCameraRotationAxisYaw);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 2
            id = "non-collidable security 2";
            viewportDictionaryKey = "column0 row1";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 45, AppData.SecurityCameraRotationSpeedMedium, new Vector3(1, 1, 0));
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 3
            id = "non-collidable security 3";
            viewportDictionaryKey = "column0 row2";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 30, AppData.SecurityCameraRotationSpeedFast, new Vector3(4, 1, 0));
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //track camera 1
            id = "non-collidable track 1";
            viewportDictionaryKey = "column0 row3";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            viewportDictionaryKey = "column0 row4";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;
            controller = new RailController(id + " controller", ControllerType.Rail, this.drivableBoxObject, this.railDictionary["rail1 - parallel to x-axis"]);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);
        }

        private void InitializeCollidableFirstPersonDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            string id = "";
            string viewportDictionaryKey = "";
            float drawDepth = 0;

            id = "collidable first person camera";
            viewportDictionaryKey = "full viewport";
            //doesnt matter how high on Y-axis we start the camera since it's collidable and will fall until the capsule toches the ground plane - see AppData::CollidableCameraViewHeight
            //just ensure that the Y-axis height is slightly more than AppData::CollidableCameraViewHeight otherwise the player will rise eerily upwards at the start of the game
            //as the CDCR system pushes the capsule out of the collidable ground plane 
            transform = new Transform3D(new Vector3(100, 1.1f * AppData.CollidableCameraViewHeight, 100), -Vector3.UnitZ, Vector3.UnitY);

            Camera3D camera = new Camera3D(id, ActorType.Camera, transform,
                    ProjectionParameters.StandardDeepSixteenNine, this.viewPortDictionary[viewportDictionaryKey], drawDepth, StatusType.Update);

            //attach a CollidableFirstPersonController
            camera.AttachController(new CollidableFirstPersonCameraController(
                    camera + " controller",
                    ControllerType.CollidableFirstPerson,
                    AppData.CameraMoveKeys,
                    AppData.CollidableCameraMoveSpeed, AppData.CollidableCameraStrafeSpeed, AppData.CameraRotationSpeed,
                    this.managerParameters,
                    camera, //parent
                    AppData.CollidableCameraCapsuleRadius,
                    AppData.CollidableCameraViewHeight,
                    1, 1, //accel, decel
                    AppData.CollidableCameraMass,
                    AppData.CollidableCameraJumpHeight,
                    Vector3.Zero)); //translation offset

            this.cameraManager.Add(camera);
        }


        private void InitializeSingleScreenCameraDemo(Integer2 screenResolution)
        {
            InitializeCollidableFirstPersonDemo(screenResolution);

            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "non-collidable track 1";
            transform = new Transform3D(new Vector3(0, 0, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;

            //track animated player if it's available
            //if (this.animatedHeroPlayerObject != null)
            //    controller = new RailController(id + " controller", ControllerType.Rail, this.animatedHeroPlayerObject, this.railDictionary["rail1 - parallel to x-axis"]);
            //else
            controller = new RailController(id + " controller", ControllerType.Rail, this.heroPlayerObject, this.railDictionary["rail1 - parallel to x-axis"]);


            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);
        }

        //adds three camera from 3 different perspectives that we can cycle through
        private void InitializeSingleScreenCycleableCameraDemo(Integer2 screenResolution)
        {

            InitializeSingleScreenCameraDemo(screenResolution);

            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "non-collidable track 1";
            transform = new Transform3D(new Vector3(0, 0, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;
            
            controller = new RailController(id + " controller", ControllerType.Rail, this.drivableBoxObject, this.railDictionary["rail1 - parallel to x-axis"]);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

        }

        //adds a third person looking at collidable HeroPlayerObject
        private void InitializeCollidableThirdPersonDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "third person collidable";
            //doesnt matter since it will reset based on target actor data
            transform = Transform3D.Zero;

            if (this.animatedHeroPlayerObject != null)
            {
                controller = new ThirdPersonController(id + "ctrllr", ControllerType.ThirdPerson, this.animatedHeroPlayerObject,
                AppData.CameraThirdPersonDistance, AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                AppData.CameraThirdPersonElevationAngleInDegrees, AppData.CameraThirdPersonScrollSpeedElevatationMultiplier,
                LerpSpeed.Fast, LerpSpeed.Medium, this.mouseManager);
            }
            else
            {
                controller = new ThirdPersonController(id + "ctrllr", ControllerType.ThirdPerson, this.heroPlayerObject,
                AppData.CameraThirdPersonDistance, AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                AppData.CameraThirdPersonElevationAngleInDegrees, AppData.CameraThirdPersonScrollSpeedElevatationMultiplier,
                LerpSpeed.Fast, LerpSpeed.Medium, this.mouseManager);
            }


            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

        }
        #endregion

        #region Events

        private void InitializeEventDispatcher()
        {
            //initialize based on the expected number of events
            this.eventDispatcher = new EventDispatcher(this, 20);

            //Don't forget to add to the component list, otherwise EventDispatcher::Update will not get called
            Components.Add(this.eventDispatcher);
        }

        private void StartGame()
        {
            //will be received by the menu manager and screen manager and set the menu to be shown and game to be paused
            EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.MainMenu));

            //publish an event to set the camera
            object[] additionalEventParamsB = { "collidable first person camera 1" };
            EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
        }
        #endregion

        #region Menu

        private void AddMenuElements()
        {
            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null, clone = null;
            string sceneID = "", buttonID = "", buttonText = "";
            int verticalBtnSeparation = 50;

            #region Main Menu
            sceneID = "main menu";

            //retrieve the background texture
            texture = this.textureDictionary["menubackground"];
            //scale the texture to fit the entire screen
            Vector2 scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("mainmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add start button
            buttonID = "startbtn";
            buttonText = "Be the Beat";
            position = new Vector2(graphics.PreferredBackBufferWidth / 5f, 220);
            texture = this.textureDictionary["genericbtn2"];
            transform = new Transform2D(position,
                0, new Vector2(1.5f, 0.6f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, Color.LightPink, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.Black, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);


            //add audio button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "audiobtn";
            clone.Text = "Audio";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            //change the texture blend color
            clone.Color = Color.LightGreen;
            this.menuManager.Add(sceneID, clone);

            //add controls button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "controlsbtn";
            clone.Text = "Controls";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            //change the texture blend color
            clone.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "exitbtn";
            clone.Text = "Exit";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            //change the texture blend color
            clone.Color = Color.LightYellow;
            //store the original color since if we modify with a controller and need to reset
            clone.OriginalColor = clone.Color;
            //attach another controller on the exit button just to illustrate multi-controller approach
            clone.AttachController(new UIColorSineLerpController("colorSineLerpController", ControllerType.SineColorLerp,
                    new TrigonometricParameters(1, 0.4f, 0), Color.LightSeaGreen, Color.LightGreen));
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Audio Menu
            sceneID = "audio menu";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["audiomenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));


            //add volume up button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "volumeUpbtn";
            clone.Text = "Volume Up";
            //change the texture blend color
            clone.Color = Color.LightPink;
            this.menuManager.Add(sceneID, clone);

            //add volume down button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            clone.ID = "volumeDownbtn";
            clone.Text = "Volume Down";
            //change the texture blend color
            clone.Color = Color.LightGreen;
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            clone.ID = "volumeMutebtn";
            clone.Text = "Volume Mute";
            //change the texture blend color
            clone.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            clone.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Controls Menu
            sceneID = "controls menu";

            //retrieve the controls menu background texture
            texture = this.textureDictionary["controlsmenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 9 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            clone.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);
            #endregion
        }

        private void AddUIElements()
        {
            //InitializeUIMousePointer();
            //InitializeUIProgress();
            InitializeUIScore();
        }

        private void InitializeUIMousePointer()
        {
            Texture2D texture = this.textureDictionary["reticuleDefault"];
            //show complete texture
            Microsoft.Xna.Framework.Rectangle sourceRectangle = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height);

            //listens for object picking events from the object picking manager
            UIPickingMouseObject myUIMouseObject = new UIPickingMouseObject("picking mouseObject",
                ActorType.UITexture,
                new Transform2D(Vector2.One),
                this.fontDictionary["mouse"],
                "",
                new Vector2(0, 40),
                texture,
                this.mouseManager,
                this.eventDispatcher);
            this.uiManager.Add(myUIMouseObject);
        }

        private void InitializeUIProgress()
        {
            float separation = 20; //spacing between progress bars

            Transform2D transform = null;
            Texture2D texture = null;
            UITextureObject textureObject = null;
            Vector2 position = Vector2.Zero;
            Vector2 scale = Vector2.Zero;
            float verticalOffset = 20;
            int startValue;

            texture = this.textureDictionary["progress_gradient"];
            scale = new Vector2(1, 0.75f);

            #region Player 1 Progress Bar
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f - texture.Width * scale.X - separation, verticalOffset);
            transform = new Transform2D(position, 0, scale,
                Vector2.Zero, /*new Vector2(texture.Width/2.0f, texture.Height/2.0f),*/
                new Integer2(texture.Width, texture.Height));

            textureObject = new UITextureObject(AppData.PlayerOneProgressID,
                    ActorType.UIDynamicTexture,
                    StatusType.Drawn | StatusType.Update,
                    transform, Color.Green,
                    SpriteEffects.None,
                    1,
                    texture);

            //add a controller which listens for pickupeventdata send when the player (or red box) collects the box on the left
            startValue = 0; //just a random number between 0 and max to demonstrate we can set initial progress value
            textureObject.AttachController(new UIProgressController(AppData.PlayerOneProgressControllerID, ControllerType.UIProgress, startValue, 10, this.eventDispatcher));
            this.uiManager.Add(textureObject);
            #endregion


            #region Player 2 Progress Bar
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f + separation, verticalOffset);
            transform = new Transform2D(position, 0, scale, Vector2.Zero, new Integer2(texture.Width, texture.Height));

            textureObject = new UITextureObject(AppData.PlayerTwoProgressID,
                    ActorType.UIDynamicTexture,
                    StatusType.Drawn | StatusType.Update,
                    transform,
                    Color.Red,
                    SpriteEffects.None,
                    0,
                    texture);

            //add a controller which listens for pickupeventdata send when the player (or red box) collects the box on the left
            startValue = 7; //just a random number between 0 and max to demonstrate we can set initial progress value
            textureObject.AttachController(new UIProgressController(AppData.PlayerTwoProgressControllerID, ControllerType.UIProgress, startValue, 10, this.eventDispatcher));
            this.uiManager.Add(textureObject);
            #endregion
        }

        private void InitializeUIScore()
        {

            float separation = 20;
            //throw new NotImplementedException();
            Transform2D transform = null;
            Texture2D texture = null;
            UITextureObject textureObject = null;
            Vector2 position = new Vector2(0,0);
            Vector2 scale = new Vector2(0, 0);
            float verticalOffset = 20;

            texture = this.textureDictionary["scoreui"];
            scale = new Vector2(0.75f, 0.75f);

            position = new Vector2(graphics.PreferredBackBufferWidth / 5.0f + separation, verticalOffset);
            transform = new Transform2D(position, 0, scale, Vector2.Zero, new Integer2(texture.Width, texture.Height));

            textureObject = new UITextureObject(AppData.PlayerOneScore,
                    ActorType.UIStaticTexture,
                    StatusType.Drawn,
                    transform, Color.Plum,
                    SpriteEffects.None,
                    1,
                    texture);
            this.uiManager.Add(textureObject);


            // SCORE NUMBER
            SpriteFont FontOrigin = this.fontDictionary["debug"];

            position = new Vector2(graphics.PreferredBackBufferWidth / 3.15f + separation, verticalOffset * 2.5f);
            transform = new Transform2D(position, 0, scale * 4, Vector2.Zero, new Integer2(texture.Width, texture.Height));

            UITextObject textObject = null;

            textObject = new UITextObject("score", ActorType.UIDynamicText, StatusType.Drawn | StatusType.Update,
                transform, Color.Plum, SpriteEffects.None, 1, beatScore.ToString(), FontOrigin);

            //textObject.AttachController(new UIProgressController(AppData.PlayerOneScore, ControllerType.UIProgress, startValue, 300, this.eventDispatcher));
            this.uiManager.Add(textObject);
        }


        private void InitializeEffects()
        {
            BasicEffect basicEffect = null;
            DualTextureEffect dualTextureEffect = null;
            Effect billboardEffect = null;


            #region Lit objects
            //create a BasicEffect and set the lighting conditions for all models that use this effect in their EffectParameters field
            basicEffect = new BasicEffect(graphics.GraphicsDevice);

            basicEffect.TextureEnabled = true;
            basicEffect.PreferPerPixelLighting = true;
            basicEffect.EnableDefaultLighting();
            this.effectDictionary.Add(AppData.LitModelsEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For Unlit objects
            //used for model objects that dont interact with lighting i.e. sky
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.LightingEnabled = false;
            this.effectDictionary.Add(AppData.UnlitModelsEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For dual texture objects
            dualTextureEffect = new DualTextureEffect(graphics.GraphicsDevice);
            this.effectDictionary.Add(AppData.UnlitModelDualEffectID, new DualTextureEffectParameters(dualTextureEffect));
            #endregion

            #region For unlit billboard objects
            billboardEffect = Content.Load<Effect>("Assets/Effects/Billboard");
            this.effectDictionary.Add(AppData.UnlitBillboardsEffectID, new BillboardEffectParameters(billboardEffect));
            #endregion

            #region For unlit primitive objects
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = true;
            this.effectDictionary.Add(AppData.UnLitPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion
        }
        #endregion

        #region Content, Update, Draw

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
//            spriteBatch = new SpriteBatch(GraphicsDevice);

//            #region Add Menu and UI
//            InitializeMenu();
//            AddMenuElements();
//            InitializeUI();
//            AddUIElements();
//            #endregion

//#if DEBUG
//            InitializeDebugTextInfo();
//#endif

        }

        protected override void UnloadContent()
        {
            // formally call garbage collection to de-allocate resources from RAM
            this.modelDictionary.Dispose();
            this.textureDictionary.Dispose();
            this.fontDictionary.Dispose();
        }

        protected override void Update(GameTime gameTime)
        {
            //exit using new gamepad manager
            if (this.gamePadManager.IsPlayerConnected(PlayerIndex.One) && this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.Back))
                this.Exit();


#if DEMO
            #region Demo
            demoSoundManager();
            demoCameraChange();
            demoAlphaChange();
            demoUIProgressUpdate();
            demoHideDebugInfo();
            demoGamePadManager();
            demoVideoDisplay();
            #endregion
#endif
            base.Update(gameTime);
        }

#if DEMO
        #region DEMO
        
        private void demoGamePadManager()
        {
            if (this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.RightTrigger))
            {
                //do something....
            }
        }
        private void demoSoundManager()
        {
            soundManager.PlayCue("gameMusic");
            
            if (this.keyboardManager.IsFirstKeyPress(Keys.T))
            {
                object[] additionalParameters = { "Thud" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                soundManager.PlayCue("Thud");
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.H) || this.keyboardManager.IsFirstKeyPress(Keys.F))
            {
                object[] additionalParameters = { "boing" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.B))
            {
                object[] additionalParameters = { "applause" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }

            /*if (BoxCollision)
            {
                object[] additionalParameters = { "Thud" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }*/

        }
        private void demoCameraChange()
        {
            //only single in single screen layout since cycling in multi-screen is meaningless
            if (this.screenManager.ScreenType == ScreenUtility.ScreenType.SingleScreen && this.keyboardManager.IsFirstKeyPress(Keys.C))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }
        }
        private void demoHideDebugInfo()
        {
            //show/hide debug info
            if (this.keyboardManager.IsFirstKeyPress(Keys.M))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnToggleDebug, EventCategoryType.Debug));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.Q))
            {
                beatScore ++;
                InitializeUIScore();
            }
        }
        private void demoUIProgressUpdate()
        {
            //testing event generation for UIProgressController
            if (this.keyboardManager.IsFirstKeyPress(Keys.O))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, new Integer(-1)};
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.L))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, new Integer(1) };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.I))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerTwoProgressControllerID, new Integer(-1)};
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.K))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerTwoProgressControllerID, new Integer(3) };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
        }
        private void demoAlphaChange()
        {
            if (this.drivableBoxObject != null)
            {
                //testing event generation on opacity change - see DrawnActor3D::Alpha setter
                if (this.keyboardManager.IsFirstKeyPress(Keys.F5))
                {
                    this.drivableBoxObject.Alpha -= 0.05f;
                }
                else if (this.keyboardManager.IsFirstKeyPress(Keys.F6))
                {
                    this.drivableBoxObject.Alpha += 0.05f;
                }
            }
        }

        private void demoVideoDisplay()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.Left))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Video, additonalParameters));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.Right))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix };
                EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.Video, additonalParameters));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.Up))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, new Integer(5) };
                EventDispatcher.Publish(new EventData(EventActionType.OnVolumeUp, EventCategoryType.Video, additonalParameters));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.Down))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, 0.05f };
                EventDispatcher.Publish(new EventData(EventActionType.OnMute, EventCategoryType.Video, additonalParameters));
            }


        }
        #endregion
#endif

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
        }
        #endregion
    }
}


