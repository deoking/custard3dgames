﻿/*Sets the texture of a parent actor based on stream of textures provided by a video
 * The video player will automtaically play any encoded audio from the video
 * To use this class we need to add video.dll in the Dependencies folder.
 * Videos should be in WMV format.*/

using GDLibrary.Actors.Drawn._3D;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Interfaces;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace GDLibrary.Controllers.GDVideo
{
    public class VideoController : GDLibrary.Controller.Base.Controller
    {
        //stores the state of the video attached to the controller
        private enum VideoState : sbyte
        {
            Playing,
            Paused,
            Stopped,
            NeverPlayed
        }

        #region Variables
        private VideoPlayer videoPlayer;
        private Video video;
        private Texture2D startTexture;
        private VideoState videoState;
        private float startVolume;

        #endregion

        #region Properties

        public VideoPlayer VideoPlayer
        {
            get
            {
                return this.videoPlayer;
            }
        }

        public Video Video
        {
            get
            {
                return this.video;
            }

            set
            {
                this.video = value;
            }
        }

        public float StartVolume
        {
            get
            {
                return this.videoPlayer.Volume;
            }

            set
            {
                this.videoPlayer.Volume = MathHelper.Clamp(value, 0, 1);
            }
        }
        #endregion

        public VideoController(string id, ControllerType controllerType, EventDispatcher eventDispatcher, Texture2D startTexture,
            Video video, float startVolume)
            :base(id, controllerType)
        {
            //video WMV file
            this.video = video;

            //set the initial texture
            this.startTexture = startTexture;

            //set the initial volume
            this.startVolume = startVolume;

            //first time
            SetVideoState(VideoState.NeverPlayed);

            //register for events
            RegisterForEventHandling(eventDispatcher);
        }

        #region Event Handling
        protected override void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.MenuChanged += EventDispatcher_MenuChanged;
            eventDispatcher.VideoChanged += EventDispatcher_VideoChanged;
        }

        //we need to explicitly tell the video object to pause if we're in the menu
        private void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start game event
            if (eventData.EventType == EventActionType.OnStart)
            {
                if (this.videoPlayer != null) //if video was paused when menu was hidden then play
                    this.videoPlayer.Play(video);
            }
            //did the event come from the main menu and is it a pause game event
            else if (eventData.EventType == EventActionType.OnPause)
            {
                if (this.videoPlayer != null) //if video was playing when menu was shown then pause
                    this.videoPlayer.Pause();
            }
        }

        private void EventDispatcher_VideoChanged(EventData eventData)
        {
            //target controller name is in the first channel of additonalParameters
            string targetControllerID = eventData.AdditionalParameters[0] as string;

            //was the event targeted at this controller
            if(targetControllerID.Equals(this.ID))
            {
                ProcessEvent(eventData);
            }
        }

        private void ProcessEvent(EventData eventData)
        {
            if(this.videoPlayer == null)
            {
                this.videoPlayer = new VideoPlayer();
                this.videoPlayer.Volume = 0.1f;
            }

            if(eventData.EventType == EventActionType.OnPlay)
            {
                if(this.videoPlayer.State != MediaState.Playing)
                {
                    this.videoPlayer.Play(video);
                    SetVideoState(VideoState.Playing);
                }
            }

            else if (eventData.EventType == EventActionType.OnPause)
            {
                if (this.videoPlayer.State == MediaState.Playing)
                {
                    this.videoPlayer.Pause();
                    SetVideoState(VideoState.Paused);
                }
            }

            else if (eventData.EventType == EventActionType.OnStop)
            {
                if (this.videoPlayer.State == MediaState.Playing || this.videoPlayer.State == MediaState.Paused)
                {
                    this.videoPlayer.Stop();
                    SetVideoState(VideoState.Stopped);
                }
            }

            else if (eventData.EventType == EventActionType.OnVolumeUp)
            {
                //volume is in the second channel of the additional parameters when we send a OnVolumeUp/Down events
                float volumeIncrement = ((eventData.AdditionalParameters[1] as Integer).Value)/100f;

                //set the property to clamp to valid values
                float newVolume = this.videoPlayer.Volume + volumeIncrement;
                if ( newVolume<= 1.0)
                {
                    this.videoPlayer.Volume = newVolume;
                }
            }

            else if (eventData.EventType == EventActionType.OnVolumeDown)
            {
                //volume is in the second channel of the additional parameters when we send a OnVolumeUp/Down events
                float volumeIncrement = (int)eventData.AdditionalParameters[1];

                //set the property to clamp to valid values
                this.videoPlayer.Volume -= volumeIncrement;
            }
        }

        #endregion
        private void SetVideoState(VideoState videoState)
        {
            this.videoState = videoState;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            ModelObject parentModelObject = actor as ModelObject;

            //set the texture if the parent is valid
            if(parentModelObject != null)
            {
                if(videoState == VideoState.Playing)
                {
                    parentModelObject.EffectParameters.Texture = videoPlayer.GetTexture();
                }
                else if (videoState == VideoState.Stopped)
                {
                    parentModelObject.EffectParameters.Texture = startTexture;
                }
            }
        }

        //dispose of the player when the controller is garbage collected - when the parent actor is removed
        public void Dispose()
        {
            this.videoPlayer.Dispose();
        }

    }
}
