﻿using GDLibrary.Parameters.Other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Controllers.Base
{
    public class SineLerpController : GDLibrary.Controller.Base.Controller
    {
        #region Fields
        private TrigonometricParameters trigonometricParameters;
        #endregion

        #region Properties
        public TrigonometricParameters TrigonometricParameters
        {
            get
            {
                return this.trigonometricParameters;
            }
            set
            {
                this.trigonometricParameters = value;
            }
        }
        #endregion

        public SineLerpController(string id, GDLibrary.Enums.ControllerType controllerType, TrigonometricParameters trigonometricParameters)
            : base(id, controllerType)
        {
            this.trigonometricParameters = trigonometricParameters;
        }
    }
}
