﻿/* Creates a simple controller for drivable objects taking input from keyboard */
using GDLibrary.Controllers.Base;
using GDLibrary.Enums;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Input;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Actors;
using Microsoft.Xna.Framework;
using GDLibrary.Interfaces;
using GDLibrary.Parameters.Other;

namespace GDLibrary.Controllers.Camera3D
{
    public class DriveController : UserInputController
    {
        #region Variables
        //Distence each move makes
        int StepSize = 190;
        int rotationAngle = 90;
        #endregion

        #region Properties
        #endregion

        public DriveController(string id, ControllerType controllerType, Keys[] moveKeys,
            float moveSpeed, float strafeSpeed, float rotationSpeed, ManagerParameters managerParameters)
            : base(id, controllerType, moveKeys, moveSpeed, strafeSpeed, rotationSpeed, managerParameters)
        {
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            base.Update(gameTime, actor);
        }

        public override void HandleKeyboardInput(GameTime gameTime, Actor3D parentActor)
        {
            //Step Amount var
            //int StepSize = 100;
            //int rotationAngle = 90;
            Vector3 translation = Vector3.Zero;
            if (this.ManagerParameters.KeyboardManager.IsFirstKeyPress(this.MoveKeys[0]))
            {
                translation =  this.MoveSpeed * StepSize *
                    parentActor.Transform3D.Look;
            }

            //rotate
            if (this.ManagerParameters.KeyboardManager.IsFirstKeyPress(this.MoveKeys[2]))
            {
                parentActor.Transform3D.RotateAroundYBy(rotationAngle);
            }
            else if (this.ManagerParameters.KeyboardManager.IsFirstKeyPress(this.MoveKeys[3]))
            {
                parentActor.Transform3D.RotateAroundYBy(-rotationAngle);
            }

            //Was a move button pressed
            if (translation != Vector3.Zero)
            {
                //remove the y-axis component of the translation
                translation.Y = 0;
                parentActor.Transform3D.TranslateBy(translation);
            }

        }
    }
}

