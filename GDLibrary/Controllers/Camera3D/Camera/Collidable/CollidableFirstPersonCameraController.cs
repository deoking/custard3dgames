﻿/* First person collidable camera controller. A collidable camera has a body and a collision skin from player object
 * but does not have any model data or any texture */

using GDLibrary.Actors;
using GDLibrary.Actors.Drawn._3D.Collidable.Player;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Input;
using GDLibrary.Managers.Screen;
using GDLibrary.Parameters.Other;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Controllers.Camera3D.Collidable
{
    public class CollidableFirstPersonCameraController : FirstPersonCameraController
    {
        #region Variables
        private PlayerObject playerObject;
        private float radius, height;
        private float accelerateRate, decelerationRate, mass, jumpHeight;
        private Vector3 translationOffset;
        #endregion

        #region Properties
        public PlayerObject PlayerObject
        {
            get
            {
                return playerObject;
            }

            set
            {
                playerObject = value;
            }
        }

        public float Radius
        {
            get
            {
                return this.radius;
            }

            set
            {
                this.radius = (value > 0) ? value : 1;
            }
        }

        public float Height
        {
            get
            {
                return this.height;
            }

            set
            {
                this.height = (value > 0) ? value : 1;
            }
        }

        public float AccelerateRate
        {
            get
            {
                return this.accelerateRate;
            }

            set
            {
                this.accelerateRate = (value >= 0) ? value : 1;
            }
        }

        public float DecelerationRate
        {
            get
            {
                return this.decelerationRate;
            }

            set
            {
                this.decelerationRate = (value >= 0) ? value : 1; ;
            }
        }

        public float Mass
        {
            get
            {
                return this.mass;
            }

            set
            {
                this.mass = (value > 0) ? value : 1;
            }
        }

        public float JumpHeight
        {
            get
            {
                return this.jumpHeight;
            }

            set
            {
                this.jumpHeight = (value > 0) ? value : 1;
            }
        }

        #endregion

        //uses the default PlayerObject as the collidable object for the camera
        public CollidableFirstPersonCameraController(string id, ControllerType controllerType, Keys[] moveKeys, float moveSpeed, float strafeSpeed, float rotationSpeed,
           ManagerParameters managerParameters,
           IActor parentActor, float radius, float height, float accelerationRate, float decelerationRate,
           float mass, float jumpHeight, Vector3 translationOffset)
           : this(id, controllerType, moveKeys, moveSpeed, strafeSpeed, rotationSpeed,
            managerParameters,
            parentActor, radius, height, accelerationRate, decelerationRate,
            mass, jumpHeight, translationOffset, null)
        {
        }

        public CollidableFirstPersonCameraController(string id, ControllerType controllerType, Keys[] moveKeys, float moveSpeed,
            float strafeSpeed, float rotationSpeed, ManagerParameters managerParameters, IActor parentActor, float radius, float height, 
            float accelerationRate, float decelerationRate, float mass, float jumpHeight, Vector3 translationOffset,
            PlayerObject collidableObject)
            :base(id, controllerType, moveKeys, moveSpeed, strafeSpeed, rotationSpeed, managerParameters)
        {
            this.Radius = radius;
            this.Height = height;
            this.AccelerateRate = accelerateRate;
            this.DecelerationRate = decelerationRate;
            this.Mass = mass;
            this.JumpHeight = jumpHeight;

            //allows us to tweak the position of the camera within the player object
            this.translationOffset = translationOffset;

            /* Create the collidable player object which comes with a collision skin and position the parentActor (i.e. the camera) inside the player object.
             * notice that we don't pass any effect, model or texture information, since in 1st person perspective we dont want to look from inside a model.
             * Therefore, we wont actually render any drawn object - the effect, texture, model (and also Color) information are unused.
             * 
             * This code allows the user to pass in their own PlayerObject (e.g. HeroPlayerObject) to be used for the collidable object basis for the camera.
             */
            if (collidableObject != null)
            {
                this.playerObject = collidableObject;
            }
            else
            {
                this.playerObject = new PlayerObject(this.ID + " - player object", ActorType.CollidableCamera, (parentActor as Actor3D).Transform3D,
                 null, null, this.MoveKeys, radius, height, accelerationRate, decelerationRate, jumpHeight,
                 translationOffset, this.ManagerParameters.KeyboardManager);
            }

            playerObject.Enable(false, mass);
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            base.Update(gameTime, actor);
        }

        public override void HandleKeyboardInput(GameTime gameTime, Actor3D parentActor)
        {
            if(parentActor != null)
            {
                //jump
                if(this.ManagerParameters.KeyboardManager.IsKeyDown(this.MoveKeys[4])) //check AppData.CameraMoveKeys for the correct index
                {
                    this.playerObject.CharacterBody.DoJump(this.jumpHeight);
                }

                //crouch
                //Check whether we should use IsKeyReleased rather than IsKeyDown
                else if (this.ManagerParameters.KeyboardManager.IsKeyDown(this.MoveKeys[5])) //check AppData.CameraMoveKeys for the correct index
                {
                    this.playerObject.CharacterBody.IsCrouching = !this.playerObject.CharacterBody.IsCrouching;
                }

                //forwards and backwards
                if (this.ManagerParameters.KeyboardManager.IsKeyDown(this.MoveKeys[0])) //check AppData.CameraMoveKeys for the correct index
                {
                    Vector3 restrictedLook = parentActor.Transform3D.Look;
                    restrictedLook.Y = 0;
                    this.playerObject.CharacterBody.Velocity += restrictedLook * this.MoveSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }

                else if (this.ManagerParameters.KeyboardManager.IsKeyDown(this.MoveKeys[1])) //check AppData.CameraMoveKeys for the correct index
                {
                    Vector3 restrictedLook = parentActor.Transform3D.Look;
                    restrictedLook.Y = 0;
                    this.playerObject.CharacterBody.Velocity -= restrictedLook * this.MoveSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }
                else //decelerate to zero when not pressed
                {
                    this.playerObject.CharacterBody.DesiredVelocity = Vector3.Zero;
                }

                //strafe left and right
                if (this.ManagerParameters.KeyboardManager.IsKeyDown(this.MoveKeys[2])) //check AppData.CameraMoveKeys for the correct index
                {
                    Vector3 restrictedRight = parentActor.Transform3D.Right;
                    restrictedRight.Y = 0;
                    this.playerObject.CharacterBody.Velocity -= restrictedRight * this.StrafeSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }

                else if (this.ManagerParameters.KeyboardManager.IsKeyDown(this.MoveKeys[3])) //check AppData.CameraMoveKeys for the correct index
                {
                    Vector3 restrictedRight = parentActor.Transform3D.Right;
                    restrictedRight.Y = 0;
                    this.playerObject.CharacterBody.Velocity += restrictedRight * this.StrafeSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }
                else //decelerate to zero when not pressed
                {
                    this.playerObject.CharacterBody.DesiredVelocity = Vector3.Zero;
                }

                //update the camera position to reflect the collision skin position
                parentActor.Transform3D.Translation = this.playerObject.CharacterBody.Position;
            }

            //hash, clone and dispose and equals
        }
    }
}
