﻿/* Enable CDCR through JigLibX by integrate the forces applied to each collidable object*/
using GDLibrary.Actors.Drawn._3D.Collidable;
using GDLibrary.Controllers.Physics;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Templates;
using JigLibX.Collision;
using JigLibX.Physics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Physics
{
    public class PhysicsManager : PausableGameComponent
    {
        #region Variables
        private PhysicsSystem physicsSystem;
        private PhysicsController physicsController;
        private float timeStep = 0;
        private List<CollidableObject> removeList;
        #endregion

        #region Properties
        public PhysicsSystem PhysicsSystem
        {
            get
            {
                return this.physicsSystem;
            }
        }

        public PhysicsController PhysicsController
        {
            get
            {
                return this.physicsController;
            }
        }
        #endregion

        //gravity pre-defined
        public PhysicsManager(Game game, EventDispatcher eventDispatcher, StatusType statusType)
            : this(game, eventDispatcher, statusType,-10 * Vector3.UnitY)
        {

        }

        //user-defined gravity
        public PhysicsManager(Game game, EventDispatcher eventDispatcher, StatusType statusType, Vector3 gravity)
            : base(game, eventDispatcher, statusType)
        {
            this.physicsSystem = new PhysicsSystem();

            //add cd/cr system
            this.physicsSystem.CollisionSystem = new CollisionSystemSAP();

            //allows us to define the direction and magnitude of gravity - default is (0, -9.8f, 0)
            this.physicsSystem.Gravity = gravity;

            //25/11/17 - prevents bug where objects would show correct CDCR response when velocity == Vector3.Zero
            this.physicsSystem.EnableFreezing = false;

            this.physicsSystem.SolverType = PhysicsSystem.Solver.Normal;
            this.physicsSystem.CollisionSystem.UseSweepTests = true;

            //affect accuracy and the overhead == time required
            this.physicsSystem.NumCollisionIterations = 8; //8
            this.physicsSystem.NumContactIterations = 8; //8
            this.physicsSystem.NumPenetrationRelaxtionTimesteps = 12; //15          

            #region SETTING_COLLISION_ACCURACY
            //affect accuracy of the collision detection
            this.physicsSystem.AllowedPenetration = 0.000025f;
            this.physicsSystem.CollisionTollerance = 0.00005f;
            #endregion

            this.physicsController = new PhysicsController();
            this.physicsSystem.AddController(physicsController);

            //batch removal - as in ObjectManager
            this.removeList = new List<CollidableObject>();

        }

        #region Event Handling
        protected override void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.RemoveActorChanged += EventDispatcher_RemoveActorChanged;
            base.RegisterForEventHandling(eventDispatcher);
        }

        private void EventDispatcher_RemoveActorChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnRemoveActor)
            {
                //using the "sender" property of the event to pass reference to object to be removed - use "as" to access Body since sender is defined as a raw object.
                Remove(eventData.Sender as CollidableObject);
            }
        }

        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start event
            if (eventData.EventCategoryType == EventCategoryType.MainMenu
                && eventData.EventType == EventActionType.OnStart)
            {
                //turn on update
                this.StatusType = StatusType.Update;
            }
            //did the event come from the game and is it a pause game event
            else if (eventData.EventCategoryType == EventCategoryType.MainMenu
                && eventData.EventType == EventActionType.OnPause)
            {
                //turn on update and draw for the menu - the game is paused
                this.StatusType = StatusType.Off;
            }
        }
        #endregion

        //call when we want to remove a drawn object from the scene
        public void Remove(CollidableObject collidableObject)
        {
            this.removeList.Add(collidableObject);
        }

        //batch remove on all objects that were requested to be removed
        protected virtual void ApplyRemove()
        {
            foreach (CollidableObject collidableObject in this.removeList)
            {
                //what would happen if we did not remove the physics body? would the CD/CR skin remain?
                this.PhysicsSystem.RemoveBody(collidableObject.Body);
            }

            this.removeList.Clear();
        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            ApplyRemove();
            timeStep = (float)gameTime.ElapsedGameTime.Ticks / TimeSpan.TicksPerSecond;
            //If the time between updates indicates an FPS of close to 60fps or less then update CDCR
            if(timeStep < 1.0f/60.0f)
            {
                physicsSystem.Integrate(timeStep);
            }
            else
            {
                //fix at 60 updates a second
                physicsSystem.Integrate(1.0f/60.0f);
            }
        }
    }
}
