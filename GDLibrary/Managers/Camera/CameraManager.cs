﻿/* Stores and organize all of the camera in the game e.g if want to use
 * split screen for multiplayer or if want a security camera*/

using GDLibrary.Actors.Camera;
using GDLibrary.Comparer;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using Microsoft.Xna.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Camera
{
    public class CameraManager : GameComponent, IEnumerable<Camera3D>
    {
        #region
        private List<Camera3D> cameraList;
        private int activeCameraIndex = -1;
        #endregion
        #region Properties
        public Camera3D ActiveCamera
        {
            get
            {
                return this.cameraList[this.activeCameraIndex];
            }
        }

        public int ActiveCameraIndex
        {
            get
            {
                return this.activeCameraIndex;
            }

            set
            {
                this.activeCameraIndex = (value >=0 && value <= this.cameraList.Count) ? value : 0;
            }
        }
        #endregion
        public CameraManager(Game game, int initialSize, EventDispatcher eventDispatcher) : base(game)
        {
            this.cameraList = new List<Camera3D>(initialSize);
            RegisterForEventHandling(eventDispatcher);
        }

        #region Event Handling
        protected void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.CameraChanged += EventDispatcher_CameraChanged;
        }

        protected void EventDispatcher_CameraChanged(EventData eventData)
        {
            //cycle to the next camera
            if (eventData.EventType == EventActionType.OnCameraCycle)
            {
                CycleActiveCamera();
            }
            else if (eventData.EventType == EventActionType.OnCameraSetActive)
            {
                //using the additional parameters channel of the event data object - ensure that the ID is set as first element in the array
                SetActiveCamera(x => x.ID.Equals(eventData.AdditionalParameters[0] as string));
            }
        }
        #endregion

        public bool SetActiveCamera(Predicate<Camera3D> predicate)
        {
            int index = this.cameraList.FindIndex(predicate);
            this.ActiveCameraIndex = index;
            return (index != -1) ? true : false;
        }

        public void CycleActiveCamera()
        {
            this.ActiveCameraIndex = (this.activeCameraIndex + 1)%this.cameraList.Count;
        }

        public void Add(Camera3D camera)
        {
            //make sure we have a default active camera first time
            if (this.cameraList.Count == 0)
            {
                this.activeCameraIndex = 0;
            }
            this.cameraList.Add(camera);
        }

        public bool Remove(Predicate<Camera3D> predicate)
        {
            Camera3D foundCamera = this.cameraList.Find(predicate);
            if(foundCamera != null)
            {
                return this.cameraList.Remove(foundCamera);
            }
            return false;
        }

        public int RemoveAll(Predicate<Camera3D> predicate)
        {
            return this.cameraList.RemoveAll(predicate);
        }

        public override void Update(GameTime gameTime)
        {
            //Update all cameras in the list. 
            foreach(Camera3D camera in this.cameraList)
            {
                camera.Update(gameTime);
            }
            base.Update(gameTime);
        }

        public void SortByDepth(SortDirectionType sortDirectionType)
        {
            this.cameraList.Sort(new CameraDepthComparer(sortDirectionType));
        }

        public IEnumerator<Camera3D> GetEnumerator()
        {
            return this.cameraList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
