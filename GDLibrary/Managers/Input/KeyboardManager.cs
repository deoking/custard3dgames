﻿/* Manage keyboard input */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Input
{
    public class KeyboardManager : GameComponent
    {
        #region Variables
        private KeyboardState oldState;
        private KeyboardState newState;
        #endregion

        #region Properties
        #endregion

        public KeyboardManager(Game game) : base(game)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            //store the old keyboard state
            this.oldState = this.newState;
            //get the current state of the keyboard
            this.newState = Keyboard.GetState();
            base.Update(gameTime);
        }

        //Is any key pressed on the keyboard
        public bool IsKeyPressed()
        {
            return (this.newState.GetPressedKeys().Length != 0);
        }

        //is a key pressed
        public bool IsKeyDown(Keys key)
        {
            return (this.newState.IsKeyDown(key));
        }

        //is a key pressed now that was not pressed in the last update
        public bool IsFirstKeyPress(Keys key)
        {
            return this.newState.IsKeyDown(key) &&
                this.oldState.IsKeyUp(key);
        }

        //has the key state changed since last update
        public bool IsStateChanged()
        {
            return !this.newState.Equals(oldState);
        }

        public bool IsAnyKeyPressed()
        {
            return this.newState.GetPressedKeys().Length == 0 ? false : true;
        }


    }
}
