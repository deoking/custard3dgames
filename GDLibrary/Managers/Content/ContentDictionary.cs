﻿/* Provide a generic map to load and store game content and allow for disposal of game content*/

using GDLibrary.Utility;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Content
{
    public class ContentDictionary<V> : IDisposable
    {
        #region Variables
        private string name;
        private Dictionary<string, V> dictionary;
        private ContentManager content;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        public Dictionary<string, V> Dictionary
        {
            get
            {
                return this.dictionary;
            }
        }

        public V this[string key]
        {
            get
            {
                if(!this.Dictionary.ContainsKey(key))
                {
                    throw new Exception(key + " resource was not in dictionary. Have you loaded it?");
                }
                return this.dictionary[key];
            }
        }
        #endregion

        public ContentDictionary(string name, ContentManager content)
        {
            this.name = name;
            this.content = content;
            this.dictionary = new Dictionary<string, V>();
        }

        public virtual bool Load(string assetPath, string key)
        {
            if(!this.dictionary.ContainsKey(key))
            {
                this.dictionary.Add(key, this.content.Load<V>(assetPath));
                return true;
            }
            return false;
        }

        //same as Load() above but usese the assetPath to form the key
        public virtual bool Load(string assetPath)
        {
            return Load(assetPath, StringUtility.ParseNameFromPath(assetPath));
        }

        public virtual bool Unload(string key)
        {
            if(this.dictionary.ContainsKey(key))
            {
                //unload from RAM
                Dispose(this.dictionary[key]);
                //remove from dictionary
                this.dictionary.Remove(key);
                return true;
            }
            return false;
        }

        public virtual int Count()
        {
            return this.dictionary.Count;
        }

        public virtual void Dispose()
        {
            //copy the values from dictionary to list
            List<V> list = new List<V>(dictionary.Values);

            for(int i = 0; i < list.Count; i++)
            {
                Dispose(list[i]);
            }

            //empty the list
            list.Clear();

            //clear the dictionary
            this.dictionary.Clear();
        }

        public virtual void Dispose(V value)
        {
            //If a disposable object e.g texture, model, sound, font then call dispose
            if(value is IDisposable)
            {
                ((IDisposable)value).Dispose();
            }
            //If it just a C# object then set to null for garbage collection
            else
            {
                value = default(V);
            }
        }
    }
}
