﻿/* Parent interface for all object controllers. A controller
 * can be attached to an actor and modify its state e.g
 * a FirstPersonCameraController attached to a Camera3D actor will handle
 * keyboard and mouse input and translate into changes in the 
 * actors Transform3D properties */

using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
/* Represent the parent interface for all object controllers. A controller can be attached to an actor to
 * modify its state e.g. a FirstPersonCameraController attached to a Camera3D actor will handle keyboard
 * and mouse input and translate into changes in the actors Transfrom3D properties */

using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Enums;

namespace GDLibrary.Interfaces
{
    public interface IController : ICloneable
    {
        void Update(GameTime gameTime, IActor actor);
        void SetActor(IActor actor); //reset the actor controller by this controller
        string GetID(); //used when we want to interrogate a controller and see if it is "the one" that we want to enable/disable, based on ID.

        //allows us to play, pause, reset, stop a controller
        bool SetControllerPlayStatus(PlayStatusType playStatusType);
    }
}

