﻿using GDLibrary.Enums;
using GDLibrary.Events.Data;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Events.Base
{
    public class EventDispatcher : GameComponent
    {
        private static Queue<EventData> queue; //store events in arrival sequence
        private static HashSet<EventData> uniqueSet; //preventt he same event from existing on the stack for a single update cycle

        //a delegate is basically a list of function pointers
        //the function pointer comes from the object that wants
        //to be notified when the event happens
        public delegate void CameraEventHandler(EventData eventData);
        public delegate void MenuEventHandler(EventData eventData);
        public delegate void ScreenEventHandler(EventData eventData);
        public delegate void OpacityEventHandler(EventData eventData);
        public delegate void RemoveActorEventHandler(EventData eventData);
        public delegate void PlayerEventHandler(EventData eventData);
        public delegate void GlobalSoundEventHandler(EventData eventData);
        public delegate void Sound3DEventHandler(EventData eventData);
        public delegate void Sound2DEventHandler(EventData eventData);
        public delegate void ObjectPickingEventHandler(EventData eventData);
        public delegate void MouseEventHandler(EventData eventData);
        public delegate void VideoEventHandler(EventData eventData);
        public delegate void DebugEventHandler(EventData eventData);



        /* An event is either null (not happened yet) or non-null
         * When the event occures the delegate will read through its list
         * and call all of the listening functions*/

        public event CameraEventHandler CameraChanged;
        public event MenuEventHandler MenuChanged;
        public event ScreenEventHandler ScreenChanged;
        public event OpacityEventHandler OpacityChanged;
        public event RemoveActorEventHandler RemoveActorChanged;
        public event PlayerEventHandler PlayerChanged;
        public event GlobalSoundEventHandler GlobalSoundChanged;
        public event Sound3DEventHandler Sound3DChanged;
        public event Sound2DEventHandler Sound2DChanged;
        public event ObjectPickingEventHandler ObjectPickChanged;
        public event MouseEventHandler MouseChanged;
        public event VideoEventHandler VideoChanged;
        public event DebugEventHandler DebugChanged;


        public EventDispatcher(Game game, int initialSize)
            : base(game)
        {
            queue = new Queue<EventData>(initialSize);
            uniqueSet = new HashSet<EventData>(new EventDataEqualityComparer());
        }

        public static void Publish(EventData eventData)
        {
            //prevent the same event from being published multiple times
            //in a single update e.g 10x bell ring sounds
            if(!uniqueSet.Contains(eventData))
            {
                queue.Enqueue(eventData);
                uniqueSet.Add(eventData);
            }
        }
        EventData eventData;
        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < queue.Count; i++)
            {
                eventData = queue.Dequeue();
                Process(eventData);
                uniqueSet.Remove(eventData);
            }
            //stack.Clear(); ? Do we need this? Pop should remove all elements
            //uniqueSet.Clear();
            base.Update(gameTime);
        }

        private void Process(EventData eventData)
        {
            //One case for each category type
            switch(eventData.EventCategoryType)
            {
                case Enums.EventCategoryType.Camera:
                    OnCamera(eventData);
                    break;

                case Enums.EventCategoryType.MainMenu:
                    OnMenu(eventData);
                    break;

                case EventCategoryType.Debug:
                    OnDebug(eventData);
                    break;

                case EventCategoryType.Opacity:
                    OnOpacity(eventData);
                    break;

                case EventCategoryType.SystemRemove:
                    OnRemoveActor(eventData);
                    break;

                case EventCategoryType.Player:
                    OnPlayer(eventData);
                    break;

                case EventCategoryType.Sound3D:
                    OnSound3D(eventData);
                    break;

                case EventCategoryType.Sound2D:
                    OnSound2D(eventData);
                    break;

                case EventCategoryType.GlobalSound:
                    OnGlobalSound(eventData);
                    break;

                case EventCategoryType.ObjectPicking:
                    OnObjectPicking(eventData);
                    break;

                case EventCategoryType.Mouse:
                    OnMouse(eventData);
                    break;

                case EventCategoryType.Video:
                    OnVideo(eventData);
                    break;

                default:
                    break;
            }
        }

        //called when a menu change is requested
        protected virtual void OnMenu(EventData eventData)
        {
            //non-null if an object subscribed for this event
            if(MenuChanged != null)
            {
                MenuChanged(eventData);
            }
        }

        //called when a menu change is requested
        protected virtual void OnCamera(EventData eventData)
        {
            //non-null if an object subscribed for this event
            if (CameraChanged != null)
            {
                CameraChanged(eventData);
            }
        }

        //called when a debug related event occurs (e.g. show/hide debug info)
        protected virtual void OnDebug(EventData eventData)
        {
            DebugChanged?.Invoke(eventData);
        }

        //called when a drawm objects opacity changes
        protected virtual void OnOpacity(EventData eventData)
        {
            OpacityChanged?.Invoke(eventData);
        }

        //called when a drawn objects needs to be removed - see UIMouseObject::HandlePickedObject()
        protected virtual void OnRemoveActor(EventData eventData)
        {
            RemoveActorChanged?.Invoke(eventData);
        }

        //called when a player related event occurs (e.g. win, lose, health increase)
        protected virtual void OnPlayer(EventData eventData)
        {
            PlayerChanged?.Invoke(eventData);
        }

        //called when a global sound event is sent to set volume by category or mute all sounds
        protected virtual void OnGlobalSound(EventData eventData)
        {
            GlobalSoundChanged?.Invoke(eventData);
        }

        //called when a 3D sound event is sent e.g. play "boom"
        protected virtual void OnSound3D(EventData eventData)
        {
            Sound3DChanged?.Invoke(eventData);
        }

        //called when a 2D sound event is sent e.g. play "menu music"
        protected virtual void OnSound2D(EventData eventData)
        {
            Sound2DChanged?.Invoke(eventData);
        }

        //called when the PickingManager picks an object
        protected virtual void OnObjectPicking(EventData eventData)
        {
            ObjectPickChanged?.Invoke(eventData);
        }

        //called when the we want to set mouse position, appearance etc.
        protected virtual void OnMouse(EventData eventData)
        {
            MouseChanged?.Invoke(eventData);
        }

        //called when the we want to set mouse position, appearance etc.
        protected virtual void OnVideo(EventData eventData)
        {
            VideoChanged?.Invoke(eventData);
        }

    }
}
