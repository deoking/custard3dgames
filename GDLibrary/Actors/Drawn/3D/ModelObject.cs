﻿/* Allows us to draw model objects. These are the fbx files
 * that you will import from 3dsmax */
using GDLibrary.Enums;
using GDLibrary.Parameters.Effects;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Drawn._3D
{
    public class ModelObject : DrawnActor3D, ICloneable
    {
        #region Variables
        private Texture2D texture;
        private Model model;
        private Matrix[] boneTransforms;

        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }

            set
            {
                this.texture = value;
            }
        }

        public Model Model
        {
            get
            {
                return this.model;
            }

            set
            {
                this.model = value;
            }
        }

        public Matrix[] BoneTransforms
        {
            get
            {
                return this.boneTransforms;
            }

            set
            {
                this.boneTransforms = value;
            }
        }

        public BoundingSphere BoundingSphere
        {
            get
            {
                //bug fix for disappearing skybox plane - scale the bounding sphere up by 10%
                return this.model.Meshes[model.Root.Index].BoundingSphere.Transform(Matrix.CreateScale(1.5f)*this.GetWorldMatrix());
            }
        }
        #endregion

        //default draw and update settings for statusType
        public ModelObject(string id, ActorType actorType,
           Transform3D transform, EffectParameters effectParameters, Model model)
           : this(id, actorType, transform, effectParameters, model, StatusType.Update | StatusType.Drawn)
        {

        }

    public ModelObject(string id, ActorType actorType, 
            Transform3D transform, EffectParameters effectParameters, Model model, StatusType statusType)
            :base(id, actorType, transform, effectParameters, statusType)
        {
            this.texture = texture;
            this.model = model;

            /* 3DS Max models contain meshes (a table might have 
             * 5 meshes - a top and four legs) and each mesh contains a
             * bone. A bone holds the transform that says "move the 
             * mesh to a particular position". Without 5 bones in the 
             * table the table would be a mess*/
            InitializeBoneTransforms();
        }

        private void InitializeBoneTransforms()
        {
            //load bone transforms from the model and copy
            //to our array
            if(this.model != null)
            {
                this.boneTransforms = new Matrix[this.model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(this.boneTransforms);
            }
        }

        public new object Clone()
        {
            return new ModelObject("Clone - " + ID,
                this.ActorType,
                (Transform3D)this.Transform3D.Clone(),
                this.EffectParameters.GetDeepCopy(),
                this.model);
        }

        public override bool Remove()
        {
            //tag for garbage collection
            this.boneTransforms = null;
            return base.Remove();
        }


    }
}
