﻿using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Parameters.Effects;
using GDLibrary.Parameters.Transforms;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Drawn._3D.Primitives
{
    public class PrimitiveObject : DrawnActor3D
    {
        #region Variables
        private IVertexData vertexData;
        #endregion 

        #region Properties
        public IVertexData VertexData
        {
            get
            {
                return this.vertexData;
            }
            set
            {
                this.vertexData = value;
            }
        }

        public BoundingSphere BoundingSphere
        {
            get
            {
                return new BoundingSphere(this.Transform3D.Translation, this.Transform3D.Scale.Length());
            }
        }
        #endregion

        public PrimitiveObject(string id, ActorType actorType, Transform3D transform,
            EffectParameters effectParameters, StatusType statusType, IVertexData vertexData)
            : base(id, actorType, transform, effectParameters, statusType)
        {
            this.vertexData = vertexData;
        }

        public new object Clone()
        {
            PrimitiveObject actor = new PrimitiveObject("clone - " + ID, //deep
               this.ActorType, //deep
               (Transform3D)this.Transform3D.Clone(), //deep
               (EffectParameters)this.EffectParameters.Clone(), //deep
               this.StatusType, //deep
               this.vertexData); //shallow - its ok if objects refer to the same vertices

            if (this.ControllerList != null)
            {
                //clone each of the (behavioural) controllers
                foreach (IController controller in this.ControllerList)
                    actor.AttachController((IController)controller.Clone());
            }

            return actor;
        }
    }
}
