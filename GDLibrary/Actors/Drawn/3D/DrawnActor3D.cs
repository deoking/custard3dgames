﻿/* Parent class for all updateable and drawn 3D game object. 
 * Notice that Effect has been added*/

using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Parameters.Effects;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Drawn._3D
{
    public class DrawnActor3D : Actor3D, ICloneable
    {
        #region Variables
        private Effect effect;
        private EffectParameters effectParameters;
        #endregion

        #region Properties

        public EffectParameters EffectParameters
        {
            get
            {
                return this.effectParameters;
            }
            set
            {
                this.effectParameters = value;
            }
        }
        public Effect Effect
        {
            get
            {
                return this.effect;
            }

            set
            {
                this.effect = value;
            }
        }

        public float Alpha
        {
            get
            {
                return this.EffectParameters.Alpha;
            }
            set
            {
                //opaque to transparent
                if(this.EffectParameters.Alpha == 1 && value < 1)
                {
                    EventDispatcher.Publish(new EventData("OpTr", this, EventActionType.OnOpaqueToTransparent, 
                        EventCategoryType.Opacity));
                }
                //transparent to opaque
                else if (this.EffectParameters.Alpha < 1 && value == 1)
                {
                    EventDispatcher.Publish(new EventData("TrOp", this, EventActionType.OnTransparentToOpaque,
                        EventCategoryType.Opacity));
                }
                this.EffectParameters.Alpha = value;

            }
        }

        #endregion

        //used when I don't want to specify a color and an alpha
        public DrawnActor3D(string id, ActorType actorType, 
            Transform3D transform, EffectParameters effectParameters) 
            : this(id, actorType, transform, effectParameters, StatusType.Drawn | StatusType.Update) //drawn and updated
        {
        }

        public DrawnActor3D(string id, ActorType actorType,
            Transform3D transform, EffectParameters effectParameters, StatusType statusType)
            : base(id, actorType, transform, statusType) //drawn and updated
        {
            this.effectParameters = effectParameters;
        }

        public override float GetAlpha()
        {
            return this.effectParameters.Alpha;
        }

        public new object Clone()
        {
            return new DrawnActor3D("Clone - " + ID,
                this.ActorType,
                (Transform3D)this.Transform3D.Clone(),
                this.EffectParameters.GetDeepCopy(),   //hybrid - shallow (texture and effect) and deep (all other fields) 
                this.StatusType);
        }

    }
}
