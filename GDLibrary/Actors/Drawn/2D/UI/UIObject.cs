﻿using GDLibrary.Actors.Drawn2D;
using GDLibrary.Enums;
using GDLibrary.Parameters.Transforms;
using GDLibrary.Parameters.Other;
using GDLibrary.Utility;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary.Actors.Drawn._2D.UI
{
    public class UIObject : DrawnActor2D
    {
        #region Variables
        private StatefulBool mouseOverState;
        #endregion

        #region Properties
        public StatefulBool MouseOverState
        {
            get
            {
                return this.mouseOverState;
            }
        }
        #endregion

        public UIObject(string id, ActorType actorType, StatusType statusType, Transform2D transform, Color color,
            SpriteEffects spriteEffects, float layerDepth)
            : base(id, actorType, transform, statusType, color, spriteEffects, layerDepth)
        {
            this.mouseOverState = new StatefulBool(2);
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.mouseOverState.GetHashCode();
            hash = hash * 17 + base.GetHashCode();
            return hash;
        }

        public override bool Remove()
        {
            this.mouseOverState = null;
            return base.Remove();
        }
    }
}
