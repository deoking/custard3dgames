﻿/*Allow the user to pass in offsets to a 3D curve so that platforms can use the same
 * curve but operate out od sync by the offsets specified*/
using GDLibrary.Curve;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Curve
{
    public class Transform3DCurveOffsets : ICloneable
    {
        public static Transform3DCurveOffsets Zero =
            new Transform3DCurveOffsets(Vector3.Zero, Vector3.One, 0, 0);

        #region Variables
        private Vector3 position, scale;
        private float rotation;
        private float timeInSecs;
        #endregion

        #region Properties
        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        public Vector3 Scale
        {
            get
            {
                return this.scale;
            }

            set
            {
                this.scale = value;
            }
        }

        public float Rotation
        {
            get
            {
                return this.rotation;
            }

            set
            {
                this.rotation = value;
            }
        }

        public float TimeInSecs
        {
            get
            {
                return this.timeInSecs;
            }

            set
            {
                this.timeInSecs = value;
            }
        }

        public float TimeInMs
        {
            get
            {
                return this.timeInSecs * 1000;
            }
        }
        #endregion

        public Transform3DCurveOffsets(Vector3 position, Vector3 scale, float rotation,
            float timeInSecs)
        {
            this.position = position;
            this.scale = scale;
            this.rotation = rotation;
            this.timeInSecs = timeInSecs;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }


    //Represent a 3D point on a curve (ie position, rotation, scale) at a specified time in
    //seconds
    public class Transform3DCurve
    {
        #region Variables
        private Curve3D translationCurve, lookCurve, upCurve;
        #endregion

        public Transform3DCurve(CurveLoopType curveLoopType)
        {
            this.translationCurve = new Curve3D(curveLoopType);
            this.lookCurve = new Curve3D(curveLoopType);
            this.upCurve = new Curve3D(curveLoopType);
        }

        public void Add(Vector3 translation, Vector3 look, Vector3 up, float timeInSecs)
        {
            this.translationCurve.Add(translation, timeInSecs);
            this.lookCurve.Add(look, timeInSecs);
            this.upCurve.Add(up, timeInSecs);
        }

        public void Clear()
        {
            this.translationCurve.Clear();
            this.lookCurve.Clear();
            this.upCurve.Clear();
        }

        public void Evaluate(float timeInSecs, int precision, out Vector3 translation,
            out Vector3 look, out Vector3 up)
        {
            translation = this.translationCurve.Evaluate(timeInSecs, precision);
            look = this.lookCurve.Evaluate(timeInSecs, precision);
            up = this.upCurve.Evaluate(timeInSecs, precision);
        }
    }
}
