﻿/* This is a 1 D curve - read Microsoft Documentation on Curves in XNA HOMEWORK*/

using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Curve
{
    public class Curve1D
    {
        #region Variables
        private Microsoft.Xna.Framework.Curve curve;
        private CurveLoopType curveLoopType;
        private bool bSet;
        #endregion

        #region Properties
        public CurveLoopType CurveLoopType
        {
            get
            {
                return this.curveLoopType;
            }
        }

        public Microsoft.Xna.Framework.Curve Curve
        {
            get
            {
                return this.curve;
            }
        }
        #endregion

        public Curve1D(CurveLoopType curveLoopType)
        {
            this.curveLoopType = curveLoopType;
            this.curve = new Microsoft.Xna.Framework.Curve();
            this.curve.PreLoop = curveLoopType;
            this.curve.PostLoop = curveLoopType;
        }

        public void Add(float value, float timeInSecs)
        {
            timeInSecs *= 1000; //convert to milliseconds
            this.curve.Keys.Add(new CurveKey(timeInSecs, value));
            this.bSet = false;
            Set();
        }

        public void Set()
        {
            SetTangents(curve);
            this.bSet = true;
        }

        public void Clear()
        {
            this.curve.Keys.Clear();
        }

        public float Evaluate(float timeInSecs, int decimalPrecision)
        {
            if(!bSet)
            {
                Set();
            }
            return (float)Math.Round(this.curve.Evaluate(timeInSecs), decimalPrecision);
        }

        private void SetTangents(Microsoft.Xna.Framework.Curve curve)
        {
            CurveKey prev;
            CurveKey current;
            CurveKey next;
            int prevIndex;
            int nextIndex;

            for(int i=0; i < curve.Keys.Count; i++)
            {
                prevIndex = i - 1;
                if(prevIndex < 0)
                {
                    prevIndex = i;
                }
                nextIndex = i + 1;
                if(nextIndex == curve.Keys.Count)
                {
                    nextIndex = i;
                }

                prev = curve.Keys[prevIndex];
                next = curve.Keys[nextIndex];
                current = curve.Keys[i];
                SetCurveKeyTangent(ref prev, ref current, ref next);
                curve.Keys[i] = current;
            }
        }

        private static void SetCurveKeyTangent(ref CurveKey prev, ref CurveKey cur,
            ref CurveKey next)
        {
            float dt = next.Position - prev.Position;
            float dv = next.Value - prev.Value;
            if(Math.Abs(dv) < float.Epsilon)
            {
                cur.TangentIn = 0;
                cur.TangentOut = 0;
            }
            else
            {
                //in and out tangents are just the slope of the curve
                cur.TangentIn = dv * (cur.Position - prev.Position) / dt;
                cur.TangentOut = dv * (next.Position - cur.Position) / dt;
            }
        }


    }
}
