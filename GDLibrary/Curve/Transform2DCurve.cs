﻿/*Allow the user to pass in offsets to a 2D curve so that platforms can use the same
 * curve but operate out od sync by the offsets specified*/
using GDLibrary.Curve;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Curve
{
    public class Transform2DCurveOffsets : ICloneable
    {
        public static Transform2DCurveOffsets Zero =
            new Transform2DCurveOffsets(Vector2.Zero, Vector2.One, 0, 0);

        #region Variables
        private Vector2 translation, scale;
        private float rotation;
        private float timeInSecs;
        #endregion

        #region Properties
        public Vector2 Translation
        {
            get
            {
                return this.translation;
            }

            set
            {
                this.translation = value;
            }
        }

        public Vector2 Scale
        {
            get
            {
                return this.scale;
            }

            set
            {
                this.scale = value;
            }
        }

        public float Rotation
        {
            get
            {
                return this.rotation;
            }

            set
            {
                this.rotation = value;
            }
        }

        public float TimeInSecs
        {
            get
            {
                return this.timeInSecs;
            }

            set
            {
                this.timeInSecs = value;
            }
        }

        public float TimeInMs
        {
            get
            {
                return this.timeInSecs * 1000;
            }
        }
        #endregion

        public Transform2DCurveOffsets(Vector2 translation, Vector2 scale, float rotation,
            float timeInSecs)
        {
            this.translation = translation;
            this.scale = scale;
            this.rotation = rotation;
            this.timeInSecs = timeInSecs;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }


    //Represent a 2D point on a curve (ie position, rotation, scale) at a specified time in
    //seconds
    public class Transform2DCurve
    {
        #region Variables
        private Curve1D rotationCurve;
        private Curve2D translationCurve, scaleCurve;
        #endregion

        public Transform2DCurve(CurveLoopType curveLoopType)
        {
            this.translationCurve = new Curve2D(curveLoopType);
            this.scaleCurve = new Curve2D(curveLoopType);
            this.rotationCurve = new GDLibrary.Curve.Curve1D(curveLoopType);
        }

        //Try for HOMEWORK Add Add, Clear and Evaluate methods
        public void Add(Vector2 translation, Vector2 scale, float rotation, float timeInSecs)
        {
            this.translationCurve.Add(translation, timeInSecs);
            this.scaleCurve.Add(scale, timeInSecs);
            this.rotationCurve.Add(rotation, timeInSecs);
        }

        public void Clear()
        {
            this.translationCurve.Clear();
            this.scaleCurve.Clear();
            this.rotationCurve.Clear();
        }

        public void Evaluate(float timeInSecs, int precision, out Vector2 translation,
            out Vector2 scale, out float rotation)
        {
            translation = this.translationCurve.Evaluate(timeInSecs, precision);
            scale = this.scaleCurve.Evaluate(timeInSecs, precision);
            rotation = this.rotationCurve.Evaluate(timeInSecs, precision);
        }
    }
}
