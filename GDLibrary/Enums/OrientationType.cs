﻿/* Used when making primitive circle vertex data to say which plane to align the vertices on */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum OrientationType : sbyte
    {
        XYAxis,
        XZAxis,
        YZAxis
    }
}
