﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum PlayStatusType
    {
        //notice that we have no powers of two value assignments (unlike with StatusType) since these states are all mutually exclusive
        Play,
        Pause,
        Stop,
        Reset
    }
}
