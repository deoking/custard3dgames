﻿/* Used by actors to define the state of an actor e.g is it drawn
 * and updated or is it just updated. When the menu is onscreen
 * and semitransparent we will see the game world, but the game should
 * be in paused state. Camera is updated but not drawn. Most game
 * models are updated and drawn*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum StatusType
    {
        Drawn = 1, //2^0
        Update = 2, //2^1
        Play = 4,
        Pause = 8,
        Stop = 16,
        Reset = 32,
        Off = 64
    /* Why use powers of 2? So we can access bits
     * e.g StatusType.Updated | StatusType.Drawn - 
     * See ObjectManager::Update() or Draw*/
    }
}
