﻿/* Used by Actor to help to distinguish one type of actor
 * for another when we are going CD/CR or when we want to
 * enable or disable certain game entities*/ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum ActorType : sbyte
    {
        Player,
        Camera,
        Decorator, //architecture
        Prop, //an interactable prop e.g ammo
        UIStaticTexture, //a menu texture
        UIText, //menu text e.g Pause
        Zone, //invisible and triggers events e.g game end
        Helper, //a wireframe around an entity e.g camera, bounding box of pickup
        Billboard, //an imposter for a 3D e.g distant tree, facade of building
        UIButton,
        UIDynamicTexture,
        UIDynamicText,
        UIStaticText,
        UITexture,

        CollidableGround,
        CollidableProp,
        CollidableCamera,
        CollidableDecorator,
        CollidablePickup,
        CollidableArchitecture,
        CollidableRecording,      //audio recordings from players that can be picked up and played
        CollidableProjectile,
        CollidableDoor,
        CollidableAmmo,
        Primitive
    }
}
