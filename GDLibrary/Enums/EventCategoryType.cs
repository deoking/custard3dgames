﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum EventCategoryType : sbyte
    {
        //one category for each group of events in EventType
        MainMenu,
        Game,
        UIMenu,
        Camera,
        Player,
        NonPlayer,
        Pickup,
        Debug,
        Opacity,
        SystemRemove,
        Sound3D,
        Sound2D,
        GlobalSound,
        ObjectPicking,   //used to notify listening objects that we have picked something with the mouse
        Mouse,
        Video
    }
}
