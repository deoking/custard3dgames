﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum EventActionType
    {
        //send by audio and video
        OnPlay,
        OnPause,
        OnResume,
        OnStop,
        OnStopAll,

        //sent by menu manager
        OnStart,
        OnRestart,
        OnVolumeUp,
        OnVolumeDown,
        OnVolumeSet,
        OnVolumeChange,
        OnMute,
        OnUnMute,
        OnExit,

        //sent by the mouse or gamepad manager
        OnClick,
        OnHover,

        //sent by the camera manager
        OnCameraSetActive,
        OnCameraCycle,

        //sent by player when gains or loses health 
        OnHealthDelta,
        OnHealthChange,
        //sent to set to a specific start/end value
        OnHealthSet,

        //sent by game state manager
        OnLose,
        OnWin,

        OnPickup,
        OnOpen,
        OnClose,

        //sent whenever we want to change from single to multiscreen and vice versa
        OnScreenLayoutChange,

        //sent whenever we change the opacity of a drawn object - remember ObjectManager has two draw lists
        OnOpaqueToTransparent,
        OnTransparentToOpaque,

        //sent when we want to add/remove an Actor from the game - see UIMouseObject::HandlePickedObject()
        OnAddActor,
        OnRemoveActor,
        //used to turn debug mode on an off
        OnToggleDebug,

        //sent by object picking manager to update listeners e.g. the UI mouse ("no object selected")
        OnObjectPicked,
        OnNonePicked,

        //used to set mouse position via an event
        OnSetMousePosition,
    }
}
